﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// ファイル管理クラス
    /// </summary>
    class FileManagement
    {
        /// <summary>
        /// csvファイルから2次元配列取得
        /// </summary>
        /// <param name="filePath">読み込みたいデータのパス</param>
        /// <param name="row">行の数</param>
        /// <param name="col">列の数</param>
        /// <returns></returns>
        public double[,] loadData(string filePath, int row, int col)
        {
            var csv = new CsvHelper.CsvReader(new StreamReader(filePath));
            double[,] data = new double[row, col];

            Console.WriteLine("Reading {0}... rows = {1}, cols = {2}", filePath, row, col);
            for (int i = 0; i < row; i++)
            {
                if (!csv.Read())
                    break;
                for (int j = 0; j < col; j++)
                {
                    // 重み読み込み
                    data[i, j] = Double.Parse(csv.GetField(j));
                }
            }
            //Console.WriteLine("Reading done！");
            return data;
        }

        /// <summary>
        /// csvファイルから2次元配列取得
        /// </summary>
        /// <param name="filePass">読み込みたいデータのパス</param>
        /// <returns></returns>
        public double[,] loadData(string filePath)
        {
            var csv = new CsvHelper.CsvReader(new StreamReader(filePath));
            int row = 0;
            int col = 0;

            // csvの行数取得
            string[] lines = File.ReadAllLines(filePath);
            row = lines.Length;
            // csvの列数取得
            using (StreamReader sr = new StreamReader(filePath,
                                         Encoding.GetEncoding("utf-8")))
            {
                // ファイルから一行読み込む
                var line = sr.ReadLine();
                // 読み込んだ一行をカンマ毎に分けて配列に格納する
                var cols = line.Split(',');
                col = cols.Length;
            }

            // 読み込み
            double[,] data = new double[row, col];
            for (int i = 0; i < row; i++)
            {
                if (!csv.Read())
                    break;
                for (int j = 0; j < col; j++)
                {
                    // 重み読み込み
                    data[i, j] = Double.Parse(csv.GetField(j));
                }
            }
            return data;
        }

        /// <summary>
        /// 入力された文字列を出力する
        /// </summary>
        /// <param name="s"></param>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public void outTxt(string s, string filePath, string fileName)
        {
            fileName = fileNameCheckerTxt(fileName);
            StreamWriter sw = new StreamWriter(filePath + "/" + fileName, false, Encoding.UTF8);
            sw.Write(s);
            sw.Close();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="learning_rate">学習率</param>
        /// <param name="iters_num">更新回数</param>
        /// <param name="batch_size">ミニバッチのサイズ</param>
        /// <param name="input_dim">入力の形状</param>
        /// <param name="filePath">ファイルのパス</param>
        public void outOption(double learning_rate, int iters_num, int batch_size, int[] input_dim, string filePath, string fileName)
        {
            fileName = fileNameCheckerTxt(fileName);
            string s = "";
            s += "学習率：" + learning_rate.ToString();
            s += "\n更新回数：" + iters_num.ToString();
            s += "\nバッチサイズ ：" + batch_size.ToString();
            s += "\n入力の形状";
            for (int i = 0; i < input_dim.Length; i++)
            {
                s += "  " + input_dim[i].ToString();
            }


            StreamWriter sw = new StreamWriter(filePath + "/" + fileName, false, Encoding.UTF8);
            sw.Write(s);
            sw.Close();


        }




        ///// <summary>
        ///// ネットワークの内容をファイル出力
        ///// </summary>
        ///// <param name="network"></param>
        //public void save_Network(Hashtable network, string pass)
        //{
        //    // Hashtableの要素を一つ一つ参照
        //    foreach (DictionaryEntry n in network)
        //    {
        //        // Hashtableのキー名がファイル名
        //        string key = n.Key + ".csv";
        //        // csvファイル作成
        //        using (System.IO.FileStream hStream = System.IO.File.Create(@pass + "/" + key))
        //        {
        //            if (hStream != null)
        //                hStream.Close();
        //        }

        //        // networkの値を取得
        //        double[,] save = (double[,])network[n.Key];

        //        // 作成したファイルに値を書き込み
        //        StreamWriter sw = new StreamWriter(@pass + "/" + key, true, System.Text.Encoding.GetEncoding("utf-8"));
        //        for (int i = 0; i < save.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < save.GetLength(1); j++)
        //            {
        //                if (j != save.GetLength(1) - 1)
        //                    sw.Write(save[i, j] + ",");
        //                else
        //                    sw.Write(save[i, j]);
        //            }
        //            sw.WriteLine();
        //        }
        //        sw.Close();
        //    }
        //    Console.WriteLine("Network saved!");
        //}

        /// <summary>
        /// 重み保存（4次元）
        /// </summary>
        /// <param name="x">重み</param>
        /// <param name="pass">保存したい場所のパス</param>
        /// <param name="name">ファイル名</param>
        public void saveWeight(double[,,,] x, string pass, string name)
        {
            // 入力の形状
            List<int> original_shape = new List<int>();
            original_shape.Add(x.GetLength(0));
            original_shape.Add(x.GetLength(1));
            original_shape.Add(x.GetLength(2));
            original_shape.Add(x.GetLength(3));

            double[,] save = Calc.reshape(x, x.GetLength(0), -1); // 2次元配列に変換

            // ファイル名
            string fileName = fileNameCheckerCsv(name);
            string fileShapeName = fileNameCheckerCsv(name); // 4次元配列の場合形状を記憶しないといけない（読み込み時使用）
            fileShapeName = fileShapeName.Substring(0, fileShapeName.Length - 4);
            fileShapeName += "_shape.csv";

            // csvファイル(重み)作成
            using (System.IO.FileStream hStream = System.IO.File.Create(@pass + "/" + fileName))
            {
                if (hStream != null)
                    hStream.Close();
            }

            // 作成したファイルに値を書き込み(重み)
            StreamWriter sw = new StreamWriter(@pass + "/" + fileName, true, System.Text.Encoding.GetEncoding("utf-8"));
            for (int i = 0; i < save.GetLength(0); i++)
            {
                for (int j = 0; j < save.GetLength(1); j++)
                {
                    if (j != save.GetLength(1) - 1)
                        sw.Write(save[i, j] + ",");
                    else
                        sw.Write(save[i, j]);
                }
                sw.WriteLine();
            }

            // 入力の形状保存
            save_List(original_shape, @pass, fileShapeName);
            sw.Close();

            Console.WriteLine("Weight saved!");
        }

        /// <summary>
        /// 重み保存（2次元）
        /// </summary>
        /// <param name="x">重み</param>
        /// <param name="pass">保存したい場所のパス</param>
        /// <param name="name">ファイル名</param>
        public void saveWeight(double[,] x, string pass, string name)
        {
            string fileName = fileNameCheckerCsv(name);

            // csvファイル作成
            using (System.IO.FileStream hStream = System.IO.File.Create(@pass + "/" + fileName))
            {
                if (hStream != null)
                    hStream.Close();
            }

            // 作成したファイルに値を書き込み
            StreamWriter sw = new StreamWriter(@pass + "/" + fileName, true, System.Text.Encoding.GetEncoding("utf-8"));
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (j != x.GetLength(1) - 1)
                        sw.Write(x[i, j] + ",");
                    else
                        sw.Write(x[i, j]);
                }
                sw.WriteLine();
            }
            //Console.WriteLine("Weight saved!");

            sw.Close();
        }





        /// <summary>
        /// リストの内容を出力(int)
        /// </summary>
        /// <param name="list">出力したいリスト</param>
        /// <param name="pass">保存したい場所</param>
        /// <param name="filename">ファイル名</param>
        public void save_List(List<int> list, string pass, string name)
        {
            string fileName = fileNameCheckerCsv(name);
            using (TextWriter textWriter = File.CreateText(@pass + "/" + fileName))
            {
                var csvWriter = new CsvWriter(textWriter);
                csvWriter.WriteRecords(list);
            }

        }


        /// <summary>
        /// リストの内容を出力(double)
        /// </summary>
        /// <param name="list">出力したいリスト</param>
        /// <param name="path">保存したい場所</param>
        /// <param name="filename">ファイル名</param>
        public void save_List(List<double> list, string path, string name)
        {
            string fileName = fileNameCheckerCsv(name);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            using (TextWriter textWriter = File.CreateText(path + "/" + fileName))
            {
                var csvWriter = new CsvWriter(textWriter);
                csvWriter.WriteRecords(list);
            }

        }

        /// <summary>
        /// ファイル名に.csv がなければ追加する
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string fileNameCheckerCsv(string fileName)
        {
            if (fileName.Length <= 4)
            {
                return fileName + ".csv";
            }

            string check = fileName.Substring(fileName.Length - 4);

            if (check == ".csv")
            {
                return fileName;
            }
            else
            {
                return fileName + ".csv";
            }

        }

        /// <summary>
        /// ファイル名に.csv がなければ追加する
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string fileNameCheckerTxt(string fileName)
        {
            if (fileName.Length <= 4)
            {
                return fileName + ".txt";
            }

            string check = fileName.Substring(fileName.Length - 4);

            if (check == ".txt")
            {
                return fileName;
            }
            else
            {
                return fileName + ".txt";
            }

        }


        /// <summary>
        /// csvから重み呼び出し
        /// (4次元の場合○○_shape.csvファイルもないとうまく動かない)
        /// </summary>
        /// <param name="pass">csvファイルのパス</param>
        /// <returns></returns>
        public double[,,,] loadWeight(string pass)
        {
            double[,,,] o = { { { { 0 } } } };
            return o;
        }

        /// <summary>
        /// csvから重み呼び出し(2次元)
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        public double[,] loadWeight2D(string pass)
        {
            double[,] o = { { 0 } };
            return o;
        }


        



    }
}
