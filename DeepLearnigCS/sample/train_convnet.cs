﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class train_convnet
    {
        public void learning()
        {
            string filePass = "D:/Development/MNIST_CSV";
            string filePass2 = "C:/道路標識検出研究/ニューラルネット/入力データ/MNIST_CSV";
            filePass = filePass2;　// 研究室でやる場合

            // 処理時間計測
            var sw = new System.Diagnostics.Stopwatch();

            // 一定数学習したときの値リスト
            List<double> train_acc = new List<double>();        // 認識精度(教師データ)
            List<double> test_acc = new List<double>();         // 認識精度(テストデータ)
            List<double> loss_list = new List<double>();    // 損失関数の値

            //訓練データ、教師データ読み込み
            FileManagement f = new FileManagement();
            // Convolution 用いる場合データが多すぎるとメモリエラー起こす
            double[,] x_train = f.loadData(filePass + "/x_train.csv", 1000, 784);  // 教師データ
            double[,] t_train = f.loadData(filePass + "/t_train.csv", 1000, 1);    // 教師データのラベル
            double[,] x_test = f.loadData(filePass + "/x_test.csv", 500, 784);     // 訓練データ
            double[,] t_test = f.loadData(filePass + "/t_test.csv", 500, 1);       // 訓練データのラベル

            // Convolution用いる場合入力データは正規化しない
            x_train = Calc.multiply(x_train, 255);
            x_test = Calc.multiply(x_test, 255);

            // 正解ラベルをone-hot表現にする(1→[0,1,0,0,0,0,0,0,0,0])
            t_train = Calc.oneHot_Label(t_train);
            t_test = Calc.oneHot_Label(t_test);

            //------------------------------------------------------------------
            double learning_rate = 0.001; // 学習率
            int iters_num = 5;       // 更新の回数
            int batch_size = 1;       // バッチの大きさ
            int[] input_dim = { 1, 28, 28 }; // 入力画像の形状（チャンネル数、高さ、幅）

            // Convolution1層のパラメータ
            Hashtable conv_Param = new Hashtable();
            // 読み込み時は
            conv_Param["filter_num"] = 30;
            conv_Param["filter_size"] = 5;
            conv_Param["filter_stride"] = 1;
            conv_Param["filter_pad"] = 0;
            //------------------------------------------------------------------


            int train_size = x_train.GetLength(0);  // 教師データの数
            int current_epoch = 0;  // 何エポック終わったか
            int iter_per_epoch = Math.Max(train_size / batch_size, 1);  // エポック

            // 処理時間計測開始
            sw.Start();

            SimpleConvNet network = new SimpleConvNet(input_dim, conv_Param, 100, 10, 0.01);
            for (int i = 0; i < iters_num; i++)
            {
                if (i != 0)
                {
                    Console.Write(".");
                }

                // ミニバッチ取得
                var resTuple = Calc.randomChoice(x_train, t_train, train_size, batch_size);
                double[,] x_batch = resTuple.Item1;
                double[,] t_batch = resTuple.Item2;

                // ミニバッチの形状を画像の形状に整形
                double[,,,] x_batchImage = Calc.reshape(x_batch, x_batch.GetLength(0), input_dim[0], input_dim[1], input_dim[2]);

                // 訓練データとテストデータを画像の形状に整形
                double[,,,] x_trainImage = Calc.reshape(x_train, x_train.GetLength(0), input_dim[0], input_dim[1], input_dim[2]);
                double[,,,] x_testImage = Calc.reshape(x_test, x_test.GetLength(0), input_dim[0], input_dim[1], input_dim[2]);

                // 1エポック終わるごとに認識精度を確認
                if (i % iter_per_epoch == 0)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(current_epoch + " / " + (iters_num / iter_per_epoch) + " epoch finished");

                    // リストに損失関数、認識精度を追加
                    train_acc.Add(network.accuracy(x_trainImage, t_train));
                    test_acc.Add(network.accuracy(x_testImage, t_test));
                    loss_list.Add(Calc.sum(network.loss(x_batchImage, t_batch)));

                    // コンソールに出力
                    Console.WriteLine("Accuracy train : " + train_acc[current_epoch]);
                    Console.WriteLine("Accuracy test  : " + test_acc[current_epoch]);
                    Console.WriteLine("loss : " + loss_list[current_epoch]);

                    current_epoch++;
                    Console.Write("Learning..");
                }


                // 誤差逆伝播法で勾配求める
                network.gradient(x_batchImage, t_batch);
                network.parameterUpdate(learning_rate);

            }

            // 処理時間計測停止
            sw.Stop();

            // ネットワークの重みを保存
            //network.saveNetwork("D:/Development/Save_Test", network.networkName);
            network.saveNetwork(network.networkPath, network.networkName);
            // 損失関数、認識精度を出力
            //f.save_List(train_acc, "D:/Development/Save_Test/"+network.networkName+"/LearningProcess", "TrainAccuracy");
            //f.save_List(test_acc, "D:/Development/Save_Test/" + network.networkName + "/LearningProcess", "TestAccuracy");
            //f.save_List(loss_list, "D:/Development/Save_Test/" + network.networkName + "/LearningProcess", "LossList");
            f.save_List(train_acc, network.networkPath + "/" + network.networkName + "/LearningProcess", "TrainAccuracy");
            f.save_List(test_acc, network.networkPath + "/" + network.networkName + "/LearningProcess", "TestAccuracy");
            f.save_List(loss_list, network.networkPath + "/" + network.networkName + "/LearningProcess", "LossList");


            // 処理時間計測結果出力
            TimeSpan ts = sw.Elapsed;
            Console.WriteLine($"\n学習時間　{ts.Hours}時間 {ts.Minutes}分 {ts.Seconds}秒 {ts.Milliseconds}ミリ秒\n");

            // 更新回数やバッチサイズなどを出力
            f.outOption(learning_rate, iters_num, batch_size, input_dim, network.networkPath+"/" + network.networkName + "/LearningProcess", "option.txt");

        }


    }
}
