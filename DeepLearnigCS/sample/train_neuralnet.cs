﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// 
    /// </summary>
    class train_neuralnet
    {
        public void learning()
        {
            string filePass = "D:/Development/MNIST_CSV";
                
            // 訓練データ、教師データ読み込み
            FileManagement f = new FileManagement();
            //string filePass2 = "C:/Users/miulabo/Research/MNIST_CSV";
            //filePass = filePass2;　// 研究室でやる場合
            double[,] x_train = f.loadData(filePass + "/x_train.csv", 60000, 784);
            double[,] t_train = f.loadData(filePass + "/t_train.csv", 60000, 1);
            double[,] x_test = f.loadData(filePass + "/x_test.csv", 10000, 784);
            double[,] t_test = f.loadData(filePass + "/t_test.csv", 10000, 1);

            //--------------ハイパーパラメータ----------------------------------
            double learning_rate = 0.1; // 学習率
            int iters_num = 7001;       // 更新の回数
            int batch_size = 100;       // バッチの大きさ
            //------------------------------------------------------------------
            int train_size = x_train.GetLength(0);  // 教師データの数
            int current_epoch = 0;  // 何エポック終わったか
            int iter_per_epoch = Math.Max(train_size / batch_size, 1);  // エポック

            // 一定数学習したときの値リスト
            List<double> train_acc = new List<double>();        // 認識精度(教師データ)
            List<double> test_acc = new List<double>();         // 認識精度(テストデータ)
            List<double[]> loss_list = new List<double[]>();    // 損失関数の値


            // 正解ラベルをone-hot表現にする
            t_train = Calc.oneHot_Label(t_train);
            t_test = Calc.oneHot_Label(t_test);

            // ネットワーク初期化
            TwoLayerNet network = new TwoLayerNet(input_size: 784, hidden_size: 50, output_size: 10);

            // 学習
            for (int i = 0; i < iters_num; i++)
            {
                // ミニバッチ取得
                var resTuple = Calc.randomChoice(x_train, t_train, train_size, batch_size);
                double[,] x_batch = resTuple.Item1;
                double[,] t_batch = resTuple.Item2;

                // MNIST表示
                //ImageData img = new ImageData();
                //img.show_MNIST(Calc.oneFromTwo(x_batch,0), Calc.oneFromTwo(t_batch, 0));

    
                // 1エポック終わるごとに認識精度を確認
                if (i % iter_per_epoch == 0 )
                {
                    
                    Console.WriteLine(current_epoch + " / " + (iters_num / iter_per_epoch) + " epoch finished");
                    Console.WriteLine("Accuracy train : " + network.accuracy(x_train, t_train));
                    Console.WriteLine("Accuracy test  : " + network.accuracy(x_test, t_test));
                    loss_list.Add(network.loss(x_batch, t_batch));
                    Console.WriteLine("loss : " + Calc.sum(loss_list[current_epoch]));
                    current_epoch++;
                    //train_acc.Add(network.accuracy(x_train, t_train));
                    //test_acc.Add(network.accuracy(x_test, t_test));
                }

                // 誤差逆伝播法で勾配求める
                network.gradient(x_batch, t_batch);

                // パラメータ更新
                foreach (Layer layer in network.layerList)
                {
                    layer.update(learning_rate);
                }
            }


        }
    }
}
