﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class TwoLayerNet
    {
        // ネットワーク
        public Affine Affine1;
        public Relu Relu1;
        //public Sigmoid Sigmoid1;
        public Affine Affine2;
        public List<Layer> layerList;

        private SoftmaxWithLoss lastLayer;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="input_size">入力層のニューロンの数</param>
        /// <param name="hidden_size">隠れ層のニューロンの数</param>
        /// <param name="output_size">出力層のニューロンの数</param>
        /// <param name="weight_init_std">ガウス分布のスケール</param>
        public TwoLayerNet(int input_size, int hidden_size, int output_size, double weight_init_std = 0.01)
        {
            // 重みの初期化
            double[,] W1 = Calc.multiply(Calc.he(input_size, hidden_size,input_size), weight_init_std);
            double[,] W2 = Calc.multiply(Calc.he(hidden_size, output_size,hidden_size), weight_init_std);
            //double[,] W1 = Calc.multiply(Calc.randn(input_size, hidden_size), weight_init_std);
            //double[,] W2 = Calc.multiply(Calc.randn(hidden_size, output_size), weight_init_std);
            double[,] b1 = Calc.zeros(1, hidden_size);
            double[,] b2 = Calc.zeros(1, output_size);

            // レイヤの初期化
            Affine1 = new Affine(W1, b1);
            Relu1 = new Relu();
            //Sigmoid1 = new Sigmoid();
            Affine2 = new Affine(W2, b2);

            // 学習の時使用
            lastLayer = new SoftmaxWithLoss();

            // レイヤリストの初期化
            layerList = new List<Layer>()
            {
                Affine1,Relu1,Affine2
            };

        }

        /// <summary>
        /// 推論処理
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <returns></returns>
        public double[,] predict(double[,] x)
        {
            foreach (Layer layer in layerList)
            {
                x = layer.forward(x);
            }
            return x;
        }

        /// <summary>
        /// 損失関数の値を求める
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <param name="t">正解ラベル</param>
        /// <returns></returns>
        public double[] loss(double[,] x, double[,] t)
        {
            double[,] y = predict(x);
            return lastLayer.forward(y, t);
        }

        /// <summary>
        /// 認識精度を求める
        /// </summary>
        /// <param name="x"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public double accuracy(double[,] x, double[,] t)
        {
            int accuracy_cnt = 0;   // 推論結果と正解ラベルが一致した回数
            int missed_cnt = 0;     // ミスした回数
            double[,] y = predict(x);
            
            for (int i = 0; i < y.GetLength(0); i++)
            {
                
                if (Calc.argmax(Calc.oneFromTwo(y, i)) == Calc.argmax(Calc.oneFromTwo(t,i)))
                {
                    accuracy_cnt++;
                }
                else
                {
                    missed_cnt++;
                }

            }
            return (double)accuracy_cnt / (double)(accuracy_cnt + missed_cnt);
        }

        /// <summary>
        /// 重みパラメーターに対する勾配を求める（逆伝播）
        /// </summary>
        /// <param name="x"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public void gradient(double[,] x, double[,] t)
        {
            // forward
            loss(x, t);

            // backward
            double[,] dout = lastLayer.backward();

            // 伝播処理と逆の順番にbackwardを呼び出す
            layerList.Reverse();
            foreach (Layer layer in layerList)
            {
                dout = layer.backward(dout);
            }
            // リストの順番を元に戻す
            layerList.Reverse();
        }
    }
}
