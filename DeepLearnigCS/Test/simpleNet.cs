﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// P110用テストクラス
    /// </summary>
    public class simpleNet
    {
        public double[,] W = { { 0.47355232, 0.9977393, 0.84668094 },
                             { 0.85557411, 0.03563661, 0.69422093  } };


        public void init()
        {

        }

        public double[] predict(double[] x)
        {
            return Calc.dot(x,W);
        }

        public double loss(double[] x, double[] t)
        {
            Console.WriteLine(Calc.display(W));
            double[] z  = this.predict(x);
            double[] y  = Calc.softmax(z);
            double loss = Calc.cross_entropy_error(y, t);
            return loss;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="W">ダミー</param>
        /// <returns></returns>
        public double f(double[] W)
        {
            double[] x = { 0.6, 0.9 };
            double[] t = { 0, 0, 1 };
            return loss(x, t);
        }
    }
}
