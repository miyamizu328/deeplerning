﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class TwoLayerNetP117
    {
        // ディクショナリ変数はcsvに保存する際に用いる
        public Hashtable param; // パラメータを保持するディクショナリ変数
        public Hashtable grads; // 勾配を保持するディクショナリ変数

        public double[,] W1;
        public double[,] b1;


        public double[,] W2;
        public double[,] b2;



        /// <summary>
        /// 
        /// </summary>
        /// <param name="input_size">入力層のニューロンの数</param>
        /// <param name="hidden_size">隠れ層のニューロンの数</param>
        /// <param name="output_size">出力層のニューロンの数</param>
        /// <param name="weight_init_std"></param>
        public void init(int input_size, int hidden_size, int output_size, double weight_init_std = 0.01)
        {
            param = new Hashtable();

            // ガウス分布に従う乱数で初期化
            W1 = Calc.randn(input_size, hidden_size);
            W2 = Calc.randn(hidden_size, output_size);

            // バイアスはゼロで初期化
            b1 = Calc.zeros(1,hidden_size);
            b2 = Calc.zeros(1,output_size);

            // networkに反映
            param["W1"] = W1;
            param["b1"] = b1;
            param["W2"] = W2;
            param["b2"] = b2;

        }

        /// <summary>
        /// 認識を行う
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double[] predict(double[] x)
        {
            double[,] W1 = (double[,])param["W1"];
            double[,] W2 = (double[,])param["W2"];
            double[,] b1 = (double[,])param["b1"];
            double[,] b2 = (double[,])param["b2"];

            // 第一層
            double[] a1 = Calc.plus(Calc.dot(x, W1), Calc.oneFromTwo(b1, 0));
            double[] z1 = Calc.sigmoid(a1);

            // 第二層
            double[] a2 = Calc.plus(Calc.dot(z1, W2), Calc.oneFromTwo(b2, 0));

            // 出力層
            return Calc.softmax(a2);

        }

        /// <summary>
        /// 損失関数の値を求める
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <param name="t"></param>
        /// <returns></returns>
        public double loss(double[] x, double[] t)
        {
            double[] y = predict(x);

            return Calc.cross_entropy_error(y, t);
        }

        public Hashtable numerical_gradient(double[] x, double[] t)
        {
            // 勾配を求めるのに使う関数（引数aはダミー）
            Func<double[], double> loss_W = (a) =>
           {
               return loss(x, t);
           };

            // テスト未完了
            grads = new Hashtable();
            grads["W1"] = Calc.numerical_gradient(loss_W, (double[,])param["W1"]);
            grads["b1"] = Calc.numerical_gradient(loss_W, (double[,])param["b1"]);
            grads["W2"] = Calc.numerical_gradient(loss_W, (double[,])param["W2"]);
            grads["b2"] = Calc.numerical_gradient(loss_W, (double[,])param["b1"]);

            return grads;
        }

        /// <summary>
        /// 認識精度を求める
        /// </summary>
        /// <param name="x">テストデータ</param>
        /// <param name="t">テストデータのラベル</param>
        /// <returns></returns>
        public double accuracy(double[,] x, double[,] t)
        {

            int accuracy_cnt = 0;   // 推論結果とラベルが一致した回数
            int missed_cnt = 0;     // ミスした回数

            for (int i = 0; i < x.GetLength(0); i++)
            {
                double[] y = predict(Calc.oneFromTwo(x, i));
                double yMax = Calc.argmax(y);
                double tMax = Calc.argmax(Calc.oneFromTwo(t, i));
                if (yMax == tMax)
                {
                    accuracy_cnt++;
                }
                else
                {
                    missed_cnt++;
                }
            }
            return (double)accuracy_cnt / (x.GetLength(0)) * 100;
        }



    }
}
