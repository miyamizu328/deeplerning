﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    // 加算レイヤ
    class AddLayer
    {
     
        public AddLayer()
        {

        }

        public double forward(double x, double y)
        {
            double o = x + y;
            return o;
        }

        public double[] backward(double dout)
        {
            double dx = dout * 1;
            double dy = dout * 1;
            double[] d = { dx, dy };
            return d;
        }

    }
}
