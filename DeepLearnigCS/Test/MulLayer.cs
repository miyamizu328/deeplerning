﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// 乗算レイヤ
    /// </summary>
    class MulLayer
    {
        public double x;
        public double y;

        public MulLayer() {
            
        }

        public double forward(double x, double y) {
            this.x = x;
            this.y = y;
            double o = x * y;
            
            return o;
        }

        public double[] backward(double dout) {
            double dx = dout * this.y;
            double dy = dout * this.x;
            Console.WriteLine("x:"+x);
            double[] o = { dx, dy };
            return o;
        }

    }
}
