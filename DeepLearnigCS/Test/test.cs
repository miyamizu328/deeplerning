﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// テスト用クラス（実際の処理とは関係ない）
    /// </summary>
    class test
    {
        public double func1(double x)
        {
            return 0.01 * Math.Pow(x, 2) + 0.1 * x;
        }

        public void d_test(string path,string networkName)
        {
            try
            {
                if (Directory.Exists(path+"/" + networkName))
                {
                    Console.WriteLine("That path exists already.");
                    return;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path + "/" + networkName);
                Console.WriteLine("The directory was created successfully ");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }




        public double func2(double[] x)
        {
            return (Math.Pow(x[0], 2) + Math.Pow(x[1], 2));
        }


        public void f_test()
        {
            FileManagement f = new FileManagement();
            List<double> test = new List<double>();
            //List<double[]> testD = new List<double[]>();
            //double[] a = { 1, 2, 3, 1 };
            //double[] b = { 5, 2, 5, 1,5 };
            //testD.Add(a);
            //testD.Add(b);
            test.Add(2);
            test.Add(3);
            test.Add(4);

            f.save_List(test, "C:/Owt/Research/save_test","test");
            //f.save_List(testD, "C:/Owt/Research/save_test", "testD");
        }




        public void reshape_test()
        {
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0,4},
                        {0,1,2,4,5},
                        {1,0,4,2,6},
                        {3,2,0,1,3}
                    },
                    {
                        {3,0,6,5,2},
                        {4,2,4,3,3}, 
                        {3,0,1,0,2},
                        {2,3,3,1,3}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }
                }
               ,
                {
                    {
                        {5,7,3,0,1},
                        {0,1,9,4,0},
                        {1,7,4,2,2},
                        {1,2,0,6,8}
                    },
                    {
                        {7,4,6,5,1},
                        {4,2,6,3,3},
                        {8,0,7,0,4},
                        {2,2,3,8,7}
                    },
                    {
                        {1,5,4,3,3},
                        {1,3,0,7,4},
                        {7,6,4,1,3},
                        {5,2,4,9,9}
                    }

                }
               // ,
               // {
               //     {
               //         {5,7,3,0},
               //         {0,1,9,4},
               //         {1,7,4,2},
               //         {1,2,0,6}
               //     },
               //     {
               //         {7,4,6,5},
               //         {4,2,6,3},
               //         {8,0,7,0},
               //         {2,2,3,8}
               //     },
               //     {
               //         {1,5,4,3},
               //         {1,3,0,7},
               //         {7,6,4,1},
               //         {5,2,4,9}
               //     }

               // }

           };

            double[,] tmp = Calc.reshape(x, 2, -1);
            Console.WriteLine(Calc.display(tmp));
            Console.WriteLine("-----------------------");
            Console.WriteLine(Calc.display(Calc.reshape(tmp,x.GetLength(0),x.GetLength(1),x.GetLength(2),x.GetLength(3))));

            FileManagement f = new FileManagement();
            f.saveWeight(x, "C:/Owt/Research/save_test", "test");



            double[,] x2d = {{1,5,6,7,8 },
                             {3,1,4,3,7 },
                             {8,5,3,4,6 },
                             {9,5,4,1,5
                } };
            f.saveWeight(x2d, "C:/Owt/Research/save_test", "test2d");
            //Console.WriteLine(Calc.display(Calc.reshape(x2d,2,2,1,-1)));
            //Console.WriteLine(Calc.display(Calc.reshape(x2d, 1, -1)));

        }




        public void sum_test()
        {
            double[,] x = { {1,5,6 },
                            {3,1,4 }, 
                            {8,5,3 }};
            Console.WriteLine(Calc.display(Calc.sum(x, axis:1)));
        }

        public void col2im_test()
        {
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0},
                        {0,1,2,4},
                        {1,0,4,2},
                        {3,2,0,1}
                    },
                    {
                        {3,0,6,5},
                        {4,2,4,3},
                        {3,0,1,0},
                        {2,3,3,1}
                    },
                    {
                        {4,2,1,2},
                        {0,1,0,4},
                        {3,0,6,2},
                        {4,2,4,5}
                    }
                }
               ,
                {
                    {
                        {5,7,3,0},
                        {0,1,9,4},
                        {1,7,4,2},
                        {1,2,0,6}
                    },
                    {
                        {7,4,6,5},
                        {4,2,6,3},
                        {8,0,7,0},
                        {2,2,3,8}
                    },
                    {
                        {1,5,4,3},
                        {1,3,0,7},
                        {7,6,4,1},
                        {5,2,4,9}
                    }

                }
                ,
                {
                    {
                        {5,7,3,0},
                        {0,1,9,4},
                        {1,7,4,2},
                        {1,2,0,6}
                    },
                    {
                        {7,4,6,5},
                        {4,2,6,3},
                        {8,0,7,0},
                        {2,2,3,8}
                    },
                    {
                        {1,5,4,3},
                        {1,3,0,7},
                        {7,6,4,1},
                        {5,2,4,9}
                    }

                }

           };
            int[] input_shape = {3,3,4,4 };
            double[,] tmp = Calc.im2col(x, 2, 2, 1, 0);
            Console.WriteLine(Calc.display(tmp));
            x = Calc.col2im(tmp, input_shape, 2, 2, 1, 0);
            Console.WriteLine(Calc.display(x));

            /*
            1, 4, 6, 0
            0, 4, 8, 8
            2, 0,16, 4
            3, 4, 0, 1

            3, 0,12, 5
            8, 8,16, 6
            6, 0, 4, 0
            2, 6, 6, 1

            4, 4, 2, 2
            0, 4, 0, 8
            6, 0,24, 4
            4, 4, 8, 5
            

            5,14, 6, 0
            0, 4,36, 8
            2,28,16, 4
            1, 4, 0, 6

            7, 8,12, 5
            8, 8,24, 6
           16, 0,28, 0
            2, 4, 6, 0

            1,10, 8, 3
            2,12, 0,14
           14,24,16, 2
            5, 4, 8, 9


            */

        }

        public void pool_test()
        {
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0,4,8},
                        {0,1,2,7,9,2},
                        {1,0,4,2,3,1},
                        {3,2,0,9,1,2}
                    },
                    {
                        {3,0,6,5,2,3},
                        {4,2,4,3,4,5},
                        {3,0,1,0,3,6},
                        {2,3,5,1,7,7}
                    },
                    {
                        {4,2,5,2,6,8},
                        {0,1,0,4,4,1},
                        {3,0,3,2,1,1},
                        {4,2,4,5,3,6}
                    },
                    {
                        {4,2,5,2,6,8},
                        {0,1,0,4,4,1},
                        {3,0,3,2,1,1},
                        {4,2,4,5,3,6}
                    }
                },
                {
                    {
                        {5,7,3,0,1,1},
                        {0,1,9,4,2,4},
                        {1,7,4,2,3,3},
                        {1,2,0,6,7,3}
                    },
                    {
                        {7,4,6,5,1,1},
                        {4,2,6,3,8,2},
                        {8,0,7,0,4,5},
                        {2,2,3,8,5,7}
                    },
                    {
                        {1,5,4,3,8,9},
                        {1,3,0,7,7,1},
                        {7,6,4,1,1,2},
                        {5,2,4,7,9,3}
                    },
                    {
                        {4,2,5,2,6,8},
                        {0,1,0,4,4,1},
                        {3,0,3,2,1,1},
                        {4,2,4,5,3,6}
                    }

                }
            };
            double[,,,] xc1 =
            {
                {

                    {

                        {5,7,3,0 },
                        {0,1,9,4 },
                        {1,7,4,2 },
                        {2,2,3,9 }
                    }
                },
                {
                    {
                        {4,1,2,9 },
                        {3,1,5,5 },
                        {7,4,8,9 },
                        {0,1,4,5 },
                    }

                }
            };

            Pooling pl = new Pooling(2, 2, 2, 0);
            double[,,,] tmp = pl.forward(x);
            Console.WriteLine(Calc.display(tmp));
            bool[,,,] mask = pl.mask;
            for (int i = 0; i < mask.GetLength(0); i++)
            {
                for (int j = 0; j < mask.GetLength(1); j++)
                {
                    for (int k = 0; k < mask.GetLength(2); k++)
                    {
                        for (int l = 0; l < mask.GetLength(3); l++)
                        {
                            Console.Write(mask[i, j, k, l] + " ");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }


            double[,,,] inback = Calc.randn(tmp.GetLength(0), tmp.GetLength(1), tmp.GetLength(2), tmp.GetLength(3));
            double[,,,] tmp2 = pl.backward(inback);
            
            Console.WriteLine(Calc.display(tmp2));
            //double[,,,] tmp1 = pl.forward(xc1);
            //Console.WriteLine(Calc.display(tmp1));
            //bool[,,,] mask = pl.mask;
            //for (int i = 0; i < mask.GetLength(0); i++)
            //{
            //    for (int j = 0; j < mask.GetLength(1); j++)
            //    {
            //        for (int k = 0; k < mask.GetLength(2); k++)
            //        {
            //            for (int l = 0; l < mask.GetLength(3); l++)
            //            {
            //                Console.Write(mask[i,j,k,l]+" ");
            //            }
            //            Console.WriteLine();
            //        }
            //        Console.WriteLine();
            //    }
            //    Console.WriteLine();
            //}

        }

        public void transpose_test()
        {
            //double[,,,] x = new double[,,,]
            //{
            //    {
            //        {
            //            {1,2,3,0},
            //            {0,1,2,4},
            //            {1,0,4,2},
            //            {3,2,0,1}
            //        },
            //        {
            //            {3,0,6,5},
            //            {4,2,4,3},
            //            {3,0,1,0},
            //            {2,3,3,1}
            //        },
            //        {
            //            {4,2,1,2},
            //            {0,1,0,4},
            //            {3,0,6,2},
            //            {4,2,4,5}
            //        }
            //    },
            //    {
            //        {
            //            {5,7,3,0},
            //            {0,1,9,4},
            //            {1,7,4,2},
            //            {1,2,0,6}
            //        },
            //        {
            //            {7,4,6,5},
            //            {4,2,6,3},
            //            {8,0,7,0},
            //            {2,2,3,8}
            //        },
            //        {
            //            {1,5,4,3},
            //            {1,3,0,7},
            //            {7,6,4,1},
            //            {5,2,4,9}
            //        }

            //    }
            //};
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0,4},
                        {0,1,2,4,5},
                        {1,0,4,2,6},
                        {3,2,0,1,3}
                    },
                    {
                        {3,0,6,5,2},
                        {4,2,4,3,3},
                        {3,0,1,0,2},
                        {2,3,3,1,0}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }
                },
                {
                    {
                        {5,7,3,0,1},
                        {0,1,9,4,0},
                        {1,7,4,2,2},
                        {1,2,0,6,8}
                    },
                    {
                        {7,4,6,5,1},
                        {4,2,6,3,3},
                        {8,0,7,0,4},
                        {2,2,3,8,7}
                    },
                    {
                        {1,5,4,3,3},
                        {1,3,0,7,4},
                        {7,6,4,1,3},
                        {5,2,4,9,9}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }

                },
                {
                    {
                        {5,7,3,0,1},
                        {0,1,9,4,0},
                        {1,7,4,2,2},
                        {1,2,0,6,8}
                    },
                    {
                        {7,4,6,5,1},
                        {4,2,6,3,3},
                        {8,0,7,0,4},
                        {2,2,3,8,7}
                    },
                    {
                        {1,5,4,3,3},
                        {1,3,0,7,4},
                        {7,6,4,1,3},
                        {5,2,4,9,9}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }

                }
            };

            double[,] x2d = {{1,5,6,7,8 },
                             {3,1,4,3,7 },
                             {8,5,3,4,6 },
                             {9,5,4,1,5 }};


            Console.WriteLine("----------------0,2,3,1----------------");
            double[,,,] t = Calc.transpose(x, 0, 2, 3, 1);
            Console.WriteLine(Calc.display(t));
            Console.WriteLine("----------------0,3,1,2----------------");
            Console.WriteLine(Calc.display(Calc.transpose(x,0,3,1,2)));
            Console.WriteLine("-------------------2d transpose-------------------");
            Console.WriteLine((Calc.display(Calc.transpose(x2d))));

           

        }


        public void col_test()
        {
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0,4},
                        {0,1,2,4,5},
                        {1,0,4,2,6},
                        {3,2,0,1,3}
                    },
                    {
                        {3,0,6,5,2},
                        {4,2,4,3,3},
                        {3,0,1,0,2},
                        {2,3,3,1,0}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }
                },
                {
                    {
                        {5,7,3,0,1},
                        {0,1,9,4,0},
                        {1,7,4,2,2},
                        {1,2,0,6,8}
                    },
                    {
                        {7,4,6,5,1},
                        {4,2,6,3,3},
                        {8,0,7,0,4},
                        {2,2,3,8,7}
                    },
                    {
                        {1,5,4,3,3},
                        {1,3,0,7,4},
                        {7,6,4,1,3},
                        {5,2,4,5,9}
                    },
                    {
                        {4,2,1,2,9},
                        {0,1,0,4,7},
                        {3,0,6,2,4},
                        {4,2,4,5,7}
                    }

                }
            };
            Console.WriteLine(x.Length);
            double[,] tmp = Calc.im2col(x, 4, 4, 1, 0);
            Console.WriteLine(Calc.display(tmp));
            Console.WriteLine(Calc.shape(tmp));
            Console.WriteLine("ーーーーーーーーーーーーーーー");
            int[] input_shape = { 2,4,4,5 };
            Console.WriteLine(Calc.display(Calc.col2im(tmp,input_shape,2,2,1,0)));
            //Console.WriteLine(Calc.shape(re));
            Console.WriteLine("ーーーーーーーーーーーーーーー");
            //Console.WriteLine(Calc.display(re42));
            //Console.WriteLine(Calc.shape(re42));

        }

        public void padding_test()
        {
            Console.WriteLine("Padding_test");
            double[,,,] x = new double[,,,]
            {
                {
                    {
                        {1,2,3,0},
                        {0,1,2,4},
                        {1,0,4,2},
                        {3,2,0,1}
                    },
                    {
                        {3,0,6,5},
                        {4,2,4,3},
                        {3,0,1,0},
                        {2,3,3,1}
                    },
                    {
                        {4,2,1,2},
                        {0,1,0,4},
                        {3,0,6,2},
                        {4,2,4,5}
                    }
                }
            };

            double[,,,] tmp = Calc.padding(x, 1);
            double[,,] test = Calc.threeFromFour(tmp, 0);
            Console.WriteLine(Calc.display(Calc.TwoFromThree(test, 0)));
        }




        public void p140_test()
        {
            double apple = 100;
            double apple_num = 2;
            double orange = 150;
            double orange_num = 3;
            double tax = 1.1;

            // Layer
            MulLayer mul_apple_layer = new MulLayer();
            MulLayer mul_orange_layer = new MulLayer();
            AddLayer add_apple_orange_layer = new AddLayer();
            MulLayer mul_tax_layer = new MulLayer();

            // forward
            double apple_price = mul_apple_layer.forward(apple, apple_num);
            double orange_price = mul_orange_layer.forward(orange, orange_num);
            double all_price = add_apple_orange_layer.forward(apple_price, orange_price);
            double price = mul_tax_layer.forward(all_price, tax);

            // backward
            double dprice = 1;
            double[] dall_dtax = mul_tax_layer.backward(dprice);
            double[] dapp_dop = add_apple_orange_layer.backward(dall_dtax[0]);
            double[] dor_dornum = mul_orange_layer.backward(dapp_dop[1]);
            double[] dap_appnum = mul_apple_layer.backward(dapp_dop[0]);

            Console.WriteLine(price);
            Console.WriteLine(dap_appnum[1] + "," + dap_appnum[0] + "," + dor_dornum[0] + "," + dor_dornum[1] + "," + dall_dtax[1]);

        }

        public void p138_test()
        {
            double apple = 100.0;
            double apple_num = 2.0;
            double tax = 1.1;

            // layer
            MulLayer mul_apple_layer = new MulLayer();
            MulLayer mul_tax_layer = new MulLayer();

            // forward
            double apple_price = mul_apple_layer.forward(apple, apple_num);
            double price = mul_tax_layer.forward(apple_price, tax);

            Console.WriteLine("price :" + price);

            // backward
            double dprice = 1.0;
            double[] dat = mul_tax_layer.backward(dprice);
            double[] dap = mul_apple_layer.backward(dat[0]);

            Console.WriteLine("dapple:" + dap[0] + " dapple_num : " + dap[1] + " dtax : " + dat[1]);

        }

        /// <summary>
        /// ガウス分布に従う初期値の設定のテスト
        /// </summary>
        public void p117_test()
        {
            double sum = 0.0;
            int count = 0;
            double[,] test = Calc.randn(100, 100, 2.0, 1.0);
            for (int i = 0; i < test.GetLength(0); i++)
            {
                for (int j = 0; j < test.GetLength(1); j++)
                {
                    sum += test[i, j];
                    count++;
                }
            }
            Console.Write("count:" + count);
            Console.WriteLine("xx:" + (test.GetLength(0) * test.GetLength(1)));
            double ave = (double)(sum / count);
            Console.WriteLine(ave);
        }



        public void p116_test()
        {
            Console.WriteLine("P116Test Start!");
            TwoLayerNetP117 net = new TwoLayerNetP117();
            net.init(784, 100, 10);

            Console.WriteLine(Calc.shape((double[,])net.param["W1"]));
            Console.WriteLine(Calc.shape(net.W1));
            Console.WriteLine(Calc.shape(net.b1));
            Console.WriteLine(Calc.shape(net.W2));
            Console.WriteLine(Calc.shape(net.b2));

            double[,] x = Calc.rand(100, 784); // ダミーの入力データ（100枚分）
            double[,] t = Calc.rand(100, 10);

            net.numerical_gradient(Calc.oneFromTwo(x, 0), Calc.oneFromTwo(t, 0));
            Console.WriteLine(Calc.shape((double[,])net.grads["W1"]));
        }


        public void p110_test()
        {
            Console.WriteLine("P110Test Start!");
            simpleNet net = new simpleNet();
            double[] x = { 0.6, 0.9 };
            double[] p = net.predict(x);
            Console.WriteLine(Calc.display(p));
            Console.WriteLine(Calc.argmax(p));
            double[] t = { 0, 0, 1 };   // 正解ラベル
            Console.WriteLine("loss :" + net.loss(x, t));
            double[,] dw;
            dw = Calc.numerical_gradient(net.f, net.W);
            Console.WriteLine(Calc.display(dw));
            Console.WriteLine("netW" + Calc.display(net.W));
        }

        public void p108_test()
        {
            double[] init_x = { -3.0, 4.0 };
            Console.WriteLine(Calc.display(Calc.gradient_descent(func2, init_x, 1e-10, 100)));
        }



        public void p104_test()
        {
            double[] x = { 3.0, 4.0 };
            Console.WriteLine(Calc.display(Calc.numerical_gradient(func2, x)));
        }

        public void p65_test()
        {
            Console.WriteLine("テスト");
            double[] x = new double[] { 1.0, 0.5 };
            double[,] W1 = new double[,] { { 0.1, 0.3, 0.5 }, { 0.2, 0.4, 0.6 } };
            double[] b1 = new double[] { 0.1, 0.2, 0.3 };
            double[,] W2 = new double[,] { { 0.1, 0.4 }, { 0.2, 0.5 }, { 0.3, 0.6 } };
            double[] b2 = new double[] { 0.1, 0.2 };
            double[,] W3 = new double[,] { { 0.1, 0.3 }, { 0.2, 0.4 } };
            double[] b3 = new double[] { 0.1, 0.2 };

            double[] a1 = Calc.dot(x, W1);
            a1 = Calc.plus(a1, b1);
            double[] z1 = Calc.sigmoid(a1);
            double[] a2 = Calc.dot(z1, W2);
            a2 = Calc.plus(a2, b2);
            double[] z2 = Calc.sigmoid(a2);
            double[] a3 = Calc.dot(z2, W3);
            a3 = Calc.plus(a3, b3);
            Console.WriteLine(Calc.display(a3));

        }


    }
}
