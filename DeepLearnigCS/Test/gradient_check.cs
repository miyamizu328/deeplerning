﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class gradient_check
    {


        public void p161_test()
        {
            
            string filePass = "D:/Development/MNIST_CSV";
            // 研究室でやる場合
            //string filePass2 = "C:/Users/miulabo/Research/MNIST_CSV";
            //filePass = filePass2;

            TwoLayerNet network = new TwoLayerNet(input_size: 784, hidden_size: 50, output_size: 10);

            double[,] x_train = get_data(filePass + "/x_train.csv", 10000, 784);
            double[,] t_train = get_data(filePass + "/t_train.csv", 10000, 1);
            double[,] x_test = get_data(filePass + "/x_test.csv", 10000, 784);
            double[,] t_test = get_data(filePass + "/t_test.csv", 10000, 1);


            t_train = Calc.oneHot_Label(t_train);
            t_test = Calc.oneHot_Label(t_test);

            Console.WriteLine("backprop Start!!");
            //Hashtable grad_backprop = network.gradient(x_train, t_train);
            Console.WriteLine("backprop Finished!!");
            //Console.WriteLine(Calc.display((double[])grad_backprop["b1"]));

            Console.WriteLine(network.accuracy(x_test, t_test));
            //Hashtable grad_numerical = network.numerical_gradient(x_train, t_train);
            //Console.WriteLine("numerical Finished!!");


        }

        /// <summary>
        /// csv読み込み関数
        /// </summary>
        /// <param name="filePass">読み込みたいデータのパス</param>
        /// <param name="row">行の数</param>
        /// <param name="col">列の数</param>
        /// <returns></returns>
        private double[,] get_data(string file, int row, int col)
        {
            var csv = new CsvHelper.CsvReader(new StreamReader(file));
            double[,] data = new double[row, col];

            Console.WriteLine("Reading {0}... rows = {1}, cols = {2}", file, row, col);
            for (int i = 0; i < row; i++)
            {
                if (!csv.Read())
                    break;
                for (int j = 0; j < col; j++)
                {
                    // 重み読み込み
                    data[i, j] = Double.Parse(csv.GetField(j));
                }
            }
            Console.WriteLine("Reading done！");
            return data;
        }


    }
}
