﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    // 3.6テストクラス
    class MNIST_Test
    {
        private Hashtable network;
        private string filePass = "D:/Development/MNIST_CSV";
        private string filePass2 = "C:/Users/miulabo/Research/MNIST_CSV";

        /// <summary>
        /// テスト用メソッド
        /// </summary>
        public void test()
        {
            // 研究室でやる場合
            filePass = filePass2;

            init(); //ネットワーク初期化

            // 入力データ、ラベル読み込み
            double[,] x = get_data(filePass + "/x_test.csv", 10000, 784);   // 28*28=784の画像データが10000個
            double[,] t = get_data(filePass + "/t_test.csv", 10000, 1);     // 10000個のラベル

            int accuracy_cnt = 0;   // 推論結果とラベルが一致した回数
            int missed_cnt = 0;     // ミスした回数

            for (int i = 0; i < x.GetLength(0); i++)
            {
                // 推論処理
                double[] y = predict(network, Calc.oneFromTwo(x, i));
                // 推論結果の一番確率高いインデックス取得   
                int maxIndex = Calc.argmax(y);

                Console.WriteLine("{0}: maxIndex = {1}, Probability = {2}, label = {3}", i, maxIndex, y[maxIndex], t[i, 0]);
                // 推論結果がラベルと一致しているか
                if (maxIndex == t[i, 0])
                    accuracy_cnt++;
                else
                    missed_cnt++;

                Console.WriteLine("accuracy: {0}", (double)accuracy_cnt / (i + 1));

            }
            Console.WriteLine("Predict Finished!");
            Console.WriteLine("--------Result--------");
            Console.WriteLine("accuracy : " + (double)accuracy_cnt / (x.GetLength(0)) * 100 + "%");
            Console.WriteLine("Missed : " + missed_cnt);
            Console.WriteLine("\nSave Network?  0:Y etc:N");
            string n = Console.ReadLine();
            if (n == "0")
            {
                Console.Write("Save in : ");    // 保存したい場所のパスを入力
                string pass = Console.ReadLine();
                save_Network(network, pass);
            }
        }
        
        /// <summary>
        /// ファイルからネットワーク初期化
        /// </summary>
        /// <returns></returns>
        public Hashtable init()
        {
            network = new Hashtable();

            // 重み、バイアスデータ読み込み
            double[,] W1 = get_data(filePass + "/sample_weight_W1.csv", 784, 50);
            double[,] W2 = get_data(filePass + "/sample_weight_W2.csv", 50, 100);
            double[,] W3 = get_data(filePass + "/sample_weight_W3.csv", 100, 10);
            double[,] b1 = get_data(filePass + "/sample_weight_b1.csv", 1, 50);
            double[,] b2 = get_data(filePass + "/sample_weight_b2.csv", 1, 100);
            double[,] b3 = get_data(filePass + "/sample_weight_b3.csv", 1, 10);

            // networkに反映
            network["W1"] = W1;
            network["b1"] = b1;
            network["W2"] = W2;
            network["b2"] = b2;
            network["W3"] = W3;
            network["b3"] = b3;

            return network;
        }

        /// <summary>
        /// MNIST推論処理(P76)
        /// </summary>
        /// <param name="network">ネットワーク</param>
        /// <param name="x"></param>
        /// <returns></returns>
        public double[] predict(Hashtable network, double[] x)
        {

            double[,] W1 = (double[,])network["W1"];
            double[,] W2 = (double[,])network["W2"];
            double[,] W3 = (double[,])network["W3"];
            double[,] b1 = (double[,])network["b1"];
            double[,] b2 = (double[,])network["b2"];
            double[,] b3 = (double[,])network["b3"];

            // 第一層
            double[] a1 = Calc.plus(Calc.dot(x, W1), Calc.oneFromTwo(b1, 0));
            double[] z1 = Calc.sigmoid(a1);

            // 第二層
            double[] a2 = Calc.plus(Calc.dot(z1, W2), Calc.oneFromTwo(b2, 0));
            double[] z2 = Calc.sigmoid(a2);

            // 第三層
            double[] a3 = Calc.plus(Calc.dot(z2, W3), Calc.oneFromTwo(b3, 0));

            // 出力層
            return Calc.softmax(a3);

        }

        /// <summary>
        /// 重み読み込み関数
        /// </summary>
        /// <param name="filePass">読み込みたいデータのパス</param>
        /// <param name="row">行の数</param>
        /// <param name="col">列の数</param>
        /// <returns></returns>
        private double[,] get_data(string file, int row, int col)
        {
            var csv = new CsvHelper.CsvReader(new StreamReader(file));
            double[,] data = new double[row, col];

            Console.WriteLine("Reading {0}... rows = {1}, cols = {2}", file, row, col);
            for (int i = 0; i < row; i++)
            {
                if (!csv.Read())
                    break;
                for (int j = 0; j < col; j++)
                {
                    // 重み読み込み
                    data[i, j] = Double.Parse(csv.GetField(j));
                }
            }
            Console.WriteLine("Reading done！");
            return data;
        }

        /// <summary>
        /// ネットワークの内容をファイル出力
        /// </summary>
        /// <param name="network"></param>
        private void save_Network(Hashtable network,string pass)
        {
            // Hashtableの要素を一つ一つ参照
            foreach (DictionaryEntry n in network)
            {
                // Hashtableのキー名がファイル名
                string key = n.Key + ".csv";
                // csvファイル作成
                using (System.IO.FileStream hStream = System.IO.File.Create(@pass+"/" + key))
                {
                    if (hStream != null)
                        hStream.Close();
                }

                // networkの値を取得
                double[,] save = (double[,])network[n.Key];

                // 作成したファイルに値を書き込み
                StreamWriter sw = new StreamWriter(@pass+"/" + key, true, System.Text.Encoding.GetEncoding("utf-8"));
                for (int i = 0; i < save.GetLength(0); i++)
                {
                    for (int j = 0; j < save.GetLength(1); j++)
                    {
                        if (j != save.GetLength(1) - 1)
                            sw.Write(save[i, j] + ",");
                        else
                            sw.Write(save[i, j]);
                    }
                    sw.WriteLine();
                }
                sw.Close();
            }
            Console.WriteLine("Network saved!");
        }
    }
}


