﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class Sigmoid : Layer
    {
        private double[,] o;
        private double[,,,] o4d;
        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,] forward(double[,] x)
        {
            o = Calc.sigmoid(x);
            return o;
        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <param name="dout"></param>
        /// <returns></returns>
        public override double[,] backward(double[,] dout)
        {
            double[,] one = new double[dout.GetLength(0),dout.GetLength(1)];
            for (int i = 0; i < one.GetLength(0); i++)
            {
                for (int j = 0; j < one.GetLength(1); j++)
                {
                    one[i, j] = 1.0 - o[i, j];
                }
                
            }
            double[,] dx = Calc.multiply(Calc.multiply(dout,one),o);

            return dx;
        }

        /// <summary>
        /// 伝播処理（4次元配列版）
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] forward(double[,,,] x)
        {
            o4d = Calc.sigmoid(x);
            return o4d;
        }

        /// <summary>
        /// 逆伝播処理（4次元配列版）
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] backward(double[,,,] dout)
        {
            double[,,,] one = new double[dout.GetLength(0), dout.GetLength(1),dout.GetLength(2),dout.GetLength(3)];
            for (int i = 0; i < one.GetLength(0); i++)
            {
                for (int j = 0; j < one.GetLength(1); j++)
                {
                    for (int k = 0; k < one.GetLength(2); i++)
                    {
                        for (int l = 0; j < one.GetLength(3); j++)
                        {
                            one[i, j,k,l] = 1.0 - o4d[i, j,k,l];
                        }
                    }
                            
                }

            }
            double[,,,] dx = Calc.multiply(Calc.multiply(dout, one), o4d);

            return dx;
        }

        /// <summary>
        /// Sigmoidレイヤは更新するパラメーターがないため何もしません
        /// </summary>
        /// <param name="learning_rate"></param>
        public override void update(double learning_rate){}

        public override void loadWeight()
        {
            
        }
    }
}
