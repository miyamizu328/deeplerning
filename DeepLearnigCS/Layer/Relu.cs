﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// Reluレイヤ(バッチ対応)
    /// </summary>
    class Relu : Layer
    {

        private bool[,,,] mask4D;   // 逆伝播時に使用（Convolution用いる場合）
        private bool[,] mask;       // 逆伝播時に使用

        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,] forward(double[,] x)
        {
            double[,] o = new double[x.GetLength(0), x.GetLength(1)];
            mask = new bool[x.GetLength(0), x.GetLength(1)];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    o[i, j] = Calc.relu(x[i, j]);
                    if (o[i, j] < 0.00000000000000001)
                    {
                        mask[i, j] = true;
                    }
                    else
                    {
                        mask[i, j] = false;
                    }
                }
            }

            return o;
        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <param name="dout"></param>
        /// <returns></returns>
        public override double[,] backward(double[,] dout)
        {
            for (int i = 0; i < dout.GetLength(0); i++)
            {
                for (int j = 0; j < dout.GetLength(1); j++)
                {
                    if (mask[i, j] == true)
                    {
                        dout[i, j] = 0.0;
                    }
                }
            }
            return dout;
        }

        /// <summary>
        /// 伝播処理（4次元配列版）
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] forward(double[,,,] x)
        {
            double[,,,] o = new double[x.GetLength(0), x.GetLength(1), x.GetLength(2), x.GetLength(3)];
            mask4D = new bool[x.GetLength(0), x.GetLength(1), x.GetLength(2), x.GetLength(3)];
            for (int b = 0; b < x.GetLength(0); b++)
            {
                for (int c = 0; c < x.GetLength(1); c++)
                {
                    for (int h = 0; h < x.GetLength(2); h++)
                    {
                        for (int w = 0; w < x.GetLength(3); w++)
                        {
                            o[b, c, h, w] = Calc.relu(x[b, c, h, w]);
                            if (o[b, c, h, w] < 0.000000000000000001)
                            {
                                mask4D[b, c, h, w] = true;
                            }
                            else
                            {
                                mask4D[b, c, h, w] = false;
                            }
                        }
                    }
                }
            }
            return o;
        }

        /// <summary>
        /// 逆伝播処理（4次元配列版）
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] backward(double[,,,] dout)
        {
            for (int b = 0; b < dout.GetLength(0); b++)
            {
                for (int c = 0; c < dout.GetLength(1); c++)
                {
                    for (int h = 0; h < dout.GetLength(2); h++)
                    {
                        for (int w = 0; w < dout.GetLength(3); w++)
                        {
                            if (mask4D[b, c, h, w] == true)
                            {
                                dout[b, c, h, w] = 0.0;
                            }
                        }
                    }
                }
            }
            return dout;
        }

        /// <summary>
        /// Reluレイヤは更新するパラメーターがないため何もしません
        /// </summary>
        /// <param name="learning_rate"></param>
        public override void update(double learning_rate)
        {


        }

        public override void loadWeight()
        {
            
        }
    }
}
