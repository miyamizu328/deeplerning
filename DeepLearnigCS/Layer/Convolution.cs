﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    class Convolution : Layer
    {
        // パラメーター
        private double[,,,] W;       // フィルター(チャンネル数、フィルターの高さ、幅)
        private double[,] b;         // バイアス
        private int stride;         // ストライド幅
        private int pad;            // パディング幅

        // 重み、バイアスパラメータの勾配
        private double[,,,] dw;
        private double[,] db;
       

        // 中間データ（逆伝播時使用）
        private double[,,,] x;      // 伝播時の入力のコピー
        private double[,] col;      // 伝播時の入力をim2col処理した後のコピー
        private double[,] col_w;    // im2col処理後のデータをフィルター展開した後のコピー

        

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="W"></param>
        /// <param name="b"></param>
        /// <param name="stride"></param>
        /// <param name="pad"></param>
        public Convolution(double[,,,] W, double[,] b, int stride = 1, int pad = 0)
        {
            this.W = W;
            this.b = b;
            this.stride = stride;
            this.pad = pad;

            Weight4D = W;
            Bias = b;
            Stride = stride;
            Padding = pad;

        }

        

        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] forward(double[,,,] x)
        {
            // フィルターの各次元の要素数を取得
            int FN = this.W.GetLength(0);
            int FC = this.W.GetLength(1);
            int FH = this.W.GetLength(2);
            int FW = this.W.GetLength(3);

            //　入力の各次元の要素数を取得
            int N = x.GetLength(0);
            int C = x.GetLength(1);
            int H = x.GetLength(2);
            int W = x.GetLength(3);

            // 出力サイズの計算
            int out_h = (int)(1 + ((H + (2 * pad) - FH) / stride));
            int out_w = (int)(1 + ((W + ((2 * pad) - FW)) / stride));

            // 内積するのに都合のいい形に整形
            double[,] col = Calc.im2col(x, FH, FW, stride, pad);
            double[,] col_w = Calc.transpose(Calc.reshape(this.W,FN,-1)); 

            // 内積を行いバイアスを足す
            double[,] o = Calc.dot(col, col_w);
            o = Calc.plusB(o, this.b);

            // 出力を適切な形に整形
            double[,,,] o_finished = Calc.reshape(o,N, out_h, out_w, -1);
            o_finished = Calc.transpose(o_finished, 0, 3, 1, 2);

            // 中間データ保存
            this.x = x;
            this.col = col;
            this.col_w = col_w;

            return o_finished;
        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] backward(double[,,,] dout)
        {
            int FN = W.GetLength(0);
            int C = W.GetLength(1);
            int FH = W.GetLength(2);
            int FW = W.GetLength(3);
            int[] x_shape = { x.GetLength(0), x.GetLength(1), x.GetLength(2),x.GetLength(3) };

            // 勾配の計算
            dout = Calc.transpose(dout,0, 2, 3, 1);
            double[,] dout2d = Calc.reshape(dout, -1, FN);
            double[] tmpdb = Calc.sum(dout2d, axis: 0);
            double[,] tmpdw = Calc.dot(Calc.transpose(this.col), dout2d);
            tmpdw = Calc.transpose(tmpdw);

            // 勾配保存
            this.db = Calc.twoFromOne(tmpdb);
            this.dw = Calc.reshape(tmpdw,FN, C, FH, FW);
            
            double[,] dcol = Calc.dot(dout2d, Calc.transpose(this.col_w));
            double[,,,] dx = Calc.col2im(dcol,x_shape, FH, FW, this.stride, this.pad);

            return dx;
        }

        /// <summary>
        /// パラメーター更新処理
        /// </summary>
        /// <param name="learning_rate"></param>
        public override void update(double learning_rate)
        {
            W = Calc.update(W, dw, learning_rate);
            b = Calc.update(b, db, learning_rate);

            // 更新後の値を抽象元に反映
            Weight4D = W;
            Bias = b;
        }

        //Convolution層は形状を無視した入力を想定してない
        public override double[,] forward(double[,] x){ throw new NotImplementedException();}
        public override double[,] backward(double[,] x){throw new NotImplementedException();}

        public override void loadWeight()
        {
            W = Weight4D;
            b = Bias;
        }
    }
}
