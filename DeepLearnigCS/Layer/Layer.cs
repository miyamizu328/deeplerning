﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// レイヤ系クラスの抽象元クラス
    /// </summary>
    public abstract class Layer
    {
        // パラメータ保存時に使用する変数
        public double[,] Weight;
        public double[,,,] Weight4D;
        public double[,] Bias;
        public int Stride;
        public int Padding;
        public int Pool_h;
        public int Pool_w;

        // 伝播、逆伝播に使用する関数
        public abstract double[,] forward(double[,] x);
        public abstract double[,,,] forward(double[,,,] x);
        public abstract double[,,,] backward(double[,,,] x);
        public abstract double[,] backward(double[,] x);
        public abstract void update(double learning_rate);

        // ネットワークをロードする際に使用
        public abstract void loadWeight();


    }
}
