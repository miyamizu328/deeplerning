﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// Maxプーリング
    /// </summary>
    class Pooling : Layer
    {
        private int pool_h; // プーリング適用領域の幅
        private int pool_w; // プーリング適用領域の高さ
        private int stride; // ストライド幅
        private int pad;    // パディング幅

        // 伝播時の入力の形状
        private int N;
        private int C;
        private int H;
        private int W;

        public bool[,,,] mask; // 最大値の位置（逆伝播に使用）

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="pool_h">プーリング適用領域の高さ</param>
        /// <param name="pool_w">幅</param>
        /// <param name="stride">ストライド幅</param>
        /// <param name="pad">パディング幅</param>
        public Pooling(int pool_h, int pool_w, int stride, int pad = 0)
        {
            this.pool_h = pool_h;
            this.pool_w = pool_w;
            this.stride = stride;   // ストライド幅は（入力の幅÷pool_w）を割り切れる値を入れてください
            this.pad = pad;

            // 抽象元に反映
            Pool_h = pool_h;
            Pool_w = pool_w;
            Stride = stride;
            Padding = pad;
        }


        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] forward(double[,,,] x)
        {
            // 入力の形状保存
            N = x.GetLength(0);
            C = x.GetLength(1);
            H = x.GetLength(2);
            W = x.GetLength(3);
            mask = new bool[N, C, H, W];

            // 入力の着目点
            int tarH = 0;
            int tarW = 0;

            // 出力の着目点
            int countW = 0;
            int countH = 0;

            double max;

            // 出力サイズ
            int out_h = (int)(1 + ((H - pool_h) / stride));
            int out_w = (int)(1 + ((W - pool_w) / stride));

            double[,,,] o = new double[N, C, out_h, out_w];
            for (int b = 0; b < o.GetLength(0); b++)
            {
                for (int c = 0; c < o.GetLength(1); c++)
                {
                    while ((tarH + pool_h) <= x.GetLength(2) && (tarW + pool_w) <= x.GetLength(3))
                    {
                        max = x[b, c, tarH, tarW];
                        int[] tmpIndex = new int[4];
                        tmpIndex[0] = b;
                        tmpIndex[1] = c;
                        tmpIndex[2] = tarH;
                        tmpIndex[3] = tarW;
                        double tmp = max;

                        for (int h = tarH; h < tarH + pool_h; h++)
                        {
                            for (int w = tarW; w < tarW + pool_w; w++)
                            {
                                max = Math.Max(max, x[b, c, h, w]);
                                // 最大値が更新されたとき
                                if (max != tmp)
                                {
                                    // 最大値のインデックス保存
                                    tmp = max;
                                    tmpIndex[0] = b;
                                    tmpIndex[1] = c;
                                    tmpIndex[2] = h;
                                    tmpIndex[3] = w;
                                }
                            }
                        }

                        o[b, c, countH, countW] = max;
                        mask[tmpIndex[0], tmpIndex[1], tmpIndex[2], tmpIndex[3]] = true;
                        countW++;
                        tarW += stride;
                        if (tarW + pool_w > x.GetLength(3))
                        {
                            tarW = 0;
                            countW = 0;
                            tarH += stride;
                            countH++;
                        }
                    }
                    // チャンネルが切り替わるごとに着目点初期化
                    countH = 0;
                    countW = 0;
                    tarH = 0;
                    tarW = 0;
                }
            }

            return o;


        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,,,] backward(double[,,,] dout)
        {
            double[,,,] dx = Calc.zeros(N, C, H, W);

            // doutの着目点
            int countH = 0;
            int countW = 0;

            // 出力の着目点
            int tarW = 0;
            int tarH = 0;

            for (int i = 0; i < dx.GetLength(0); i++)
            {
                for (int c = 0; c < dx.GetLength(1); c++)
                {
                    while (tarH + pool_h <= dx.GetLength(2) && tarW + pool_w <= dx.GetLength(3))
                    {
                        for (int h = tarH; h < tarH + pool_h; h++)
                        {
                            for (int w = tarW; w < tarW + pool_w; w++)
                            {
                                if (mask[i, c, h, w] == true)
                                {
                                    dx[i, c, h, w] = dout[i, c, countH, countW];
                                    countW++;
                                    if (countW >= dout.GetLength(3))
                                    {
                                        countH++;
                                        countW = 0;
                                        if (countH >= dout.GetLength(2))
                                        {
                                            countH = 0;
                                        }
                                    }
                                }
                                
                            }
                        }
                        tarW += stride;
                        if (tarW + pool_w > dx.GetLength(3))
                        {
                            tarW = 0;
                            tarH += stride;
                        }
                    }
                    countH = 0;
                    countW = 0;
                    tarH = 0;
                    tarW = 0;

                }
            }

            return dx;
        }

        /// <summary>
        /// プーリング層は更新するものがないため宣言だけです
        /// </summary>
        /// <param name="learning_rate"></param>
        public override void update(double learning_rate) { }

        // プーリング層は2次元配列の処理を必要としないので、宣言だけです
        public override double[,] forward(double[,] x) { throw new NotImplementedException(); }
        public override double[,] backward(double[,] x) { throw new NotImplementedException(); }

        public override void loadWeight()
        {
            
        }
    }
}
