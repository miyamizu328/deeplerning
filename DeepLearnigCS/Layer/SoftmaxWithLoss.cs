﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// ソフトマックス、交差エントロピー誤差レイヤ
    /// </summary>
    class SoftmaxWithLoss
    {
        private double[] loss;    // 損失
        private double[,] y;      // softmaxを適用した後の出力
        private double[,] t;      // 教師ラベル(one-hot)

        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <param name="t">教師ラベル</param>
        /// <returns></returns>
        public double[] forward(double[,] x, double[,] t)
        {
            this.t = t;
            y = Calc.softmax(x);
            loss = Calc.cross_entropy_error(y, t);

            return loss;
        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <returns></returns>
        public double[,] backward()
        {
            int batch_size = this.t.GetLength(0);
            double[,] dx = Calc.division(Calc.minus(y,t),batch_size);

            return dx;
        }

    }
}
