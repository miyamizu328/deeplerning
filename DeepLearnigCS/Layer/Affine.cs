﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// Affineレイヤ
    /// </summary>
    public class Affine : Layer
    {
        private double[,] W;     // 重みパラメータ
        private double[,] b;     // バイアス

        private double[,] x;     // 伝播処理の入力保存（逆伝播に使用）
        private double[,] dW;    // 重みの勾配
        private double[,] db;    // バイアスの勾配

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="W">重みの初期値</param>
        /// <param name="b">バイアスの初期値</param>
        public Affine(double[,] W, double[,] b)
        {
            this.W = W;
            this.b = b;

            // 抽象元に反映
            Weight = W;
            Bias = b;
        }


        /// <summary>
        /// 伝播処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double[,] forward(double[,] x)
        {
            this.x = x;
            double[,] o = Calc.plusB(Calc.dot(x, W), b);

            return o;
        }

        /// <summary>
        /// 逆伝播処理
        /// </summary>
        /// <param name="dout"></param>
        /// <returns></returns>
        public override double[,] backward(double[,] dout)
        {
            double[,] dx = Calc.dot(dout, Calc.transpose(W));
            this.dW = Calc.dot(Calc.transpose(x), dout);
            double[] tmp = Calc.sum(dout, axis: 0);
            this.db = new double[b.GetLength(0), b.GetLength(1)];
            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    this.db[i, j] = tmp[j];
                }
            }
            return dx;
        }

        /// <summary>
        /// パラメーター更新処理
        /// </summary>
        /// <param name="learning_rate"></param>
        public override void update(double learning_rate)
        {
            this.W = Calc.update(W, dW, learning_rate);
            this.b = Calc.update(b, db, learning_rate);

            // 抽象元に反映
            Weight = W;
            Bias = b;
        }

        public override double[,,,] forward(double[,,,] x)
        {
            throw new NotImplementedException();
        }

        public override double[,,,] backward(double[,,,] x)
        {
            throw new NotImplementedException();
        }

        public override void loadWeight()
        {
            W = Weight;
            b = Bias;
        }
    }
}
