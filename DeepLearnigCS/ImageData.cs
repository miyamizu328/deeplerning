﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace DeepLearnigCS
{
    /// <summary>
    /// 画像に対する処理をまとめたクラス
    /// </summary>
    class ImageData
    {
        /// <summary>
        /// 入力されたMNISTをコンソールに表示
        /// </summary>
        /// <param name="x"></param>
        /// <param name="t"></param>
        public void show_MNIST(double[] x, double[] t)
        {
            for (int i = 0; i < x.Length; i++)
            {
                if (i % 28 == 0)
                {
                    Console.WriteLine();
                }
                if (x[i] != 0)
                {
                    Console.Write("■");
                }
                else
                    Console.Write("□");
            }
            Console.WriteLine("\n" + Calc.display(t));
        }

        /// <summary>
        /// 入力された画像からRGB値を取得する
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public double[,,] getRGB(Bitmap img)
        {
            int Width = img.Width;
            int Height = img.Height;
            double[,,] image = new double[3, Height, Width];
            Color pixel;

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    pixel = img.GetPixel(j, i);
                    image[0, i, j] = pixel.R;
                    image[1, i, j] = pixel.G;
                    image[2, i, j] = pixel.B;
                }
            }

            Console.WriteLine(Calc.display(image));
            img.Dispose();
            return image;

        }

        /// <summary>
        /// 入力された画像の配列からRGB値を取得する
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public double[,,,] getRGB(Bitmap[] img)
        {
            int Width = img[0].Width;
            int Height = img[0].Height;
            double[,,,] image = new double[img.Length,3, Height, Width];
            Color pixel;
            for (int n = 0; n < img.Length; n++)
            {
                for (int i = 0; i < Height; i++)
                {
                    for (int j = 0; j < Width; j++)
                    {
                        pixel = img[n].GetPixel(j, i);
                        image[n,0, i, j] = pixel.R;
                        image[n,1, i, j] = pixel.G;
                        image[n,2, i, j] = pixel.B;
                    }
                }
            }
            //Console.WriteLine(Calc.display(image));
            return image;

        }




        /// <summary>
        /// 入力した画像を指定したサイズに拡大、縮小する
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public void resizeImage(Bitmap image,int width,int height,string filePath,string fileName)
        {
            // ImageオブジェクトのGraphicsオブジェクトを作成する
            Bitmap resizedImage = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(resizedImage);

            // 補間方法として高品質双三次補間を指定する
            g.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(image, 0, 0, width, height);

            resizedImage.Save(filePath+"/"+fileName+".bmp");

            // BitmapとGraphicsオブジェクトを破棄
            image.Dispose();
            resizedImage.Dispose();
            g.Dispose();


        }

        /// <summary>
        /// 入力した画像を指定したサイズに拡大、縮小する
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public Bitmap resizeImage(Bitmap image, int width, int height)
        {
            // ImageオブジェクトのGraphicsオブジェクトを作成する
            Bitmap resizedImage = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(resizedImage);

            // 補間方法として高品質双三次補間を指定する
            g.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(image, 0, 0, width, height);

            // BitmapとGraphicsオブジェクトを破棄
            image.Dispose();
            //resizedImage.Dispose();
            g.Dispose();

            return resizedImage;
        }

    }
}
