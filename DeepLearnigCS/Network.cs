﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    public class Network
    {
        // リストが二つあるのは、伝播処理と逆伝播処理の途中で入力の次元が変わるからである
        public List<Layer> layerListConv;
        public List<Layer> layerListAffine;

        public string networkName = "TestNet";
        public string networkPath = @"C:\道路標識検出研究\ニューラルネット\出力データ"; // 保存または読み込みネットワークフォルダのあるパス

        private SoftmaxWithLoss lastLayer;

        private int[] originalDim;  // 伝播時の形状保存用変数（逆伝播で使用）

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="input_dim">推論対象の形状（チャンネル数、高さ、幅）</param>
        /// <param name="outPutSize">推論したい対象の種類の数</param>
        /// <param name="weightInitStd">重みのスケール</param>
        /// <param name="loadNet">同じ構造の学習済みネットワークがあるか</param>
        public Network(int[] inputDim, int outPutSize, double weightInitStd, bool loadNet)
        {
            originalDim = new int[4];
            int inputChannel = inputDim[0];
            int inputSize = inputDim[1];

            //------Convolutionの初期値(配列の長さはConvolution層の数に応じて長くなる)--------
            int[] filterNum = { 30 };   // フィルターの数
            int[] filterSize = { 5 };   // フィルターの幅、高さ
            int[] filterPad = { 0 };    // パディングの幅
            int[] filterStride = { 1 }; // ストライド幅
            //--------------------------------------------------------------------------------

            // 出力のサイズ計算
            int convOutputSize0 = convOutputSize(inputSize, filterSize[0], filterPad[0], filterStride[0]);
            int poolOutputSize0 = poolOutputSize(convOutputSize0, filterNum[0]);

            // 重みの初期化
            double[,,,] W1 = Calc.randn(filterNum[0], inputChannel, filterSize[0], filterSize[0]);  // Convolution第一層
            double[,] W2 = Calc.randn(poolOutputSize0, 100);                                        // Affine 第一層
            double[,] W3 = Calc.randn(100, outPutSize);                                              // Affine 第二層

            // バイアス初期化
            double[,] b1 = Calc.zeros(1, filterNum[0]); // Convolution第一層
            double[,] b2 = Calc.zeros(1, 100);          // Affine 第一層
            double[,] b3 = Calc.zeros(1, outPutSize);   // Affine 第二層

            // 重みのスケール調整
            W1 = Calc.multiply(W1, weightInitStd);
            W2 = Calc.multiply(W2, weightInitStd);
            W3 = Calc.multiply(W3, weightInitStd);

            // レイヤの初期化
            // 最初のAffineまで
            Convolution Conv1 = new Convolution(W1, b1, filterStride[0], filterPad[0]);
            Relu Relu1 = new Relu();
            Pooling Pool1 = new Pooling(pool_h: 2, pool_w: 2, stride: 2, pad: 0);

            // 出力まで
            Affine Affine1 = new Affine(W2, b2);
            Relu Relu2 = new Relu();
            Affine Affine2 = new Affine(W3, b3);

            // 最後の層（学習時の使用）
            lastLayer = new SoftmaxWithLoss();

            // レイヤリスト初期化
            layerListConv = new List<Layer>() { Conv1, Relu1, Pool1 };          // Affine入る前まで
            layerListAffine = new List<Layer>() { Affine1, Relu2, Affine2 };    // 出力まで

            if (loadNet)
            {
                loadNetwork(networkPath + "/" + networkName);
            }

        }

        /// <summary>
        /// 推論処理
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double[,] predict(double[,,,] x)
        {
            foreach (Layer layer in layerListConv)
            {
                x = layer.forward(x);
            }
            // ここから入力の次元が違う
            // 元の形状保存
            originalDim[0] = x.GetLength(0);
            originalDim[1] = x.GetLength(1);
            originalDim[2] = x.GetLength(2);
            originalDim[3] = x.GetLength(3);
            double[,] o = Calc.reshape(x, x.GetLength(0), -1);　// 適切な形状に整形

            foreach (Layer layer in layerListAffine)
            {
                o = layer.forward(o);
            }
            return o;
        }

        /// <summary>
        /// 損失関数の値を求める
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <param name="t">正解ラベル</param>
        /// <returns>データの数の推論結果</returns>
        public double[] loss(double[,,,] x, double[,] t)
        {
            double[,] y = predict(x);
            return lastLayer.forward(y, t);
        }

        /// <summary>
        /// 認識精度を求める
        /// </summary>
        /// <param name="x">入力画像</param>
        /// <param name="t">正解ラベル</param>
        /// <returns></returns>
        public double accuracy(double[,,,] x, double[,] t)
        {
            int accuracy_cnt = 0;   // 推論結果と正解ラベルが一致した回数
            int missed_cnt = 0;     // ミスした回数
            double[,] y = predict(x);

            for (int i = 0; i < y.GetLength(0); i++)
            {

                if (Calc.argmax(Calc.oneFromTwo(y, i)) == Calc.argmax(Calc.oneFromTwo(t, i)))
                {
                    accuracy_cnt++;
                }
                else
                {
                    //Console.WriteLine("missed!  Out: "+ Calc.argmax(Calc.oneFromTwo(y, i)) + " Label :" + Calc.argmax(Calc.oneFromTwo(t, i)));
                    missed_cnt++;
                }

            }
            return (double)accuracy_cnt / (double)(accuracy_cnt + missed_cnt);
        }

        /// <summary>
        /// 重みパラメーターに対する勾配を求める
        /// </summary>
        /// <param name="x">入力データ</param>
        /// <param name="t">教師ラベル</param>
        /// <returns></returns>
        public void gradient(double[,,,] x, double[,] t)
        {
            // forward
            loss(x, t);

            // backward
            double[,] dout = lastLayer.backward();

            // 伝播処理と逆の順番にbackwardを呼び出す
            layerListAffine.Reverse();
            layerListConv.Reverse();
            foreach (Layer layer in layerListAffine)
            {
                //Console.WriteLine("affine backing...");
                dout = layer.backward(dout);
            }
            // 適切な形に整形
            double[,,,] o = Calc.reshape(dout, originalDim[0], originalDim[1], originalDim[2], originalDim[3]);

            foreach (Layer layer in layerListConv)
            {
                //Console.WriteLine("conv backing...");
                o = layer.backward(o);
            }
            // リストの順番を元に戻す
            layerListAffine.Reverse();
            layerListConv.Reverse();
        }

        /// <summary>
        /// 求めた勾配からパラメータを更新する
        /// </summary>
        /// <param name="learning_rate">学習率</param>
        public void parameterUpdate(double learning_rate)
        {
            // パラメータ更新
            foreach (Layer layer in layerListAffine)
            {
                layer.update(learning_rate);
            }
            foreach (Layer layer in layerListConv)
            {
                layer.update(learning_rate);
            }
        }

        /// <summary>
        /// ネットワークのパラメータをcsvで保存する
        /// </summary>
        /// <param name="pass">保存したい場所の</param>
        public void saveNetwork(string path, string networkName)
        {
            FileManagement f = new FileManagement();
            // 指定されたパスに指定された名前のフォルダ作成
            try
            {
                if (Directory.Exists(path + "/" + networkName))
                {

                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path + "/" + networkName);
                //Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            // ファイルパス決定
            string finalPath = path + "/" + networkName;
            string netConstruction = "";
            string check = "";
            int numConv = 0;
            int numAffine = 0;
            int numPool = 0;

            foreach (Layer layer in layerListConv)
            {
                check = layer.ToString().Substring(14);
                switch (check)
                {
                    case "Convolution":
                        f.saveWeight(layer.Weight4D, finalPath, check + numConv.ToString() + "_w");
                        f.saveWeight(layer.Bias, finalPath, check + numConv.ToString() + "_b");
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Convolution " + numConv + "\nStride : " + layer.Stride + "\nPadding : " + layer.Padding;
                        netConstruction += "\n----------------------------------\n";
                        numConv++;
                        break;
                    case "Pooling":
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Poolng " + numPool + "\nStride : " + layer.Stride + "\nPadding : " + layer.Padding;
                        netConstruction += "\nPool_h :" + layer.Pool_h + "\nPool_w : " + layer.Pool_h;
                        netConstruction += "\n----------------------------------\n";
                        numPool++;
                        break;
                    case "Relu":
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Relu";
                        netConstruction += "\n----------------------------------\n";
                        break;
                    case "Sigmoid":
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Sigmoid";
                        netConstruction += "\n----------------------------------\n";
                        break;
                    default:
                        break;
                }
            }

            foreach (Layer layer in layerListAffine)
            {
                check = layer.ToString().Substring(14);
                switch (check)
                {
                    case "Affine":
                        f.saveWeight(layer.Weight, finalPath, check + numAffine.ToString() + "_w");
                        f.saveWeight(layer.Bias, finalPath, check + numAffine.ToString() + "_b");
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Affine " + numAffine;
                        netConstruction += "\n----------------------------------\n";
                        numAffine++;
                        break;
                    case "Relu":
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Relu";
                        netConstruction += "\n----------------------------------\n";
                        break;
                    case "Sigmoid":
                        netConstruction += "\n----------------------------------\n";
                        netConstruction += "Sigmoid";
                        netConstruction += "\n----------------------------------\n";
                        break;
                    default:
                        break;
                }
            }

            // ネットワークの構造をtxtで出力（表示するときは通常のテキストエディターよりもSublimeを推奨）
            f.outTxt(netConstruction, finalPath, "netConstruction.txt");
            //StreamWriter sw = new StreamWriter(finalPath + "/netConstruction.txt", false, Encoding.UTF8);
            //sw.Write(netConstruction);
            //sw.Close();

            Console.WriteLine(netConstruction);

            Console.WriteLine("Network Saved!");

        }





        /// <summary>
        /// パラメータを読み込み(ネットワーク初期化後使用)
        /// </summary>
        /// <param name="filePath">パラメータファイルのあるパス</param>
        public void loadNetwork(string filePath)
        {
            int countConv = 0;
            int countAffine = 0;
            FileManagement f = new FileManagement();

            // 指定されたパスが存在またパラメータファイルがあるか
            if (Directory.Exists(filePath)) { }
            else
            {
                Console.WriteLine("指定されたパスは不適切です");
                return;
            }

            string check;
            foreach (Layer layer in layerListConv)
            {
                check = layer.ToString().Substring(14);
                switch (check)
                {
                    case "Convolution":
                        // 重みの形状読み込み
                        double[,] shape = f.loadData(filePath + "/Convolution" + countConv + "_w_shape.csv");

                        // 重みを読み込んで適切な形状に整形
                        layer.Weight4D = Calc.reshape(f.loadData(filePath + "/Convolution" + countConv + "_w.csv"), (int)shape[0, 0], (int)shape[1, 0], (int)shape[2, 0], (int)shape[3, 0]);

                        // バイアス読み込み
                        layer.Bias = f.loadData(filePath + "/Convolution" + countConv + "_b.csv");

                        // 読み込んだ重み、バイアスをレイヤに反映
                        layer.loadWeight();
                        countConv++;
                        break;
                    case "Pooling":
                        break;
                    case "Relu":
                        break;
                    case "Sigmoid":
                        break;
                    default:
                        break;
                }
            }

            foreach (Layer layer in layerListAffine)
            {
                check = layer.ToString().Substring(14);
                switch (check)
                {
                    case "Affine":
                        // 重みを読み込んで適切な形状に整形
                        layer.Weight = f.loadData(filePath + "/Affine" + countAffine + "_w.csv");

                        // バイアス読み込み
                        layer.Bias = f.loadData(filePath + "/Affine" + countAffine + "_b.csv");

                        // 読み込んだ重み、バイアスをレイヤに反映
                        layer.loadWeight();
                        countAffine++;
                        break;
                    case "Relu":
                        break;
                    case "Sigmoid":
                        break;
                    default:
                        break;
                }
            }

        }

        /// <summary>
        /// Convolution層の出力のサイズを返す
        /// </summary>
        /// <param name="input_size">入力の幅または高さ</param>
        /// <param name="filter_size">フィルターの数</param>
        /// <param name="filter_pad">フィルターのパディングの値</param>
        /// <param name="filter_stride">フィルターのストライドの値</param>
        /// <returns></returns>
        private int convOutputSize(int input_size, int filter_size, int filter_pad, int filter_stride)
        {
            int size = 1 + (input_size - filter_size + (2 * filter_pad)) / filter_stride;
            return size;
        }

        /// <summary>
        /// Pooling層の出力のサイズを返す
        /// </summary>
        /// <param name="input_size">前Convolution層の出力の数</param>
        /// <param name="filter_pad">前Convolution層のフィルターの数</param>       
        /// <returns></returns>
        private int poolOutputSize(int conv_output_size, int filter_num)
        {
            int size = (int)(filter_num * (conv_output_size / 2.0) * (conv_output_size / 2.0));
            return size;
        }

    }
}
