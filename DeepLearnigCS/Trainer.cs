﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace DeepLearnigCS
{
    class Trainer
    {
        int[] inputDim = new int[3];  // 入力画像の形状（チャンネル数、高さ、幅）

        private double[,,,] testData;   // テストデータ
        private double[,] testLabel;    // テストデータのラベル
        private double[,,,] trainData;  // 教師データ
        private double[,] trainLabel;   // 教師データのラベル

        public void Learning()
        {
            //------------------------------------------------
            double learning_rate = 0.001;       // 学習率
            int iters_num = 50;                // 更新の回数
            int batchSize = 10;                 // バッチの大きさ (教師データが少ないのでミニバッチ用いない)
            int outPutSize = 3;                 // 推論対象の種類の数
            inputDim[0] = 3; inputDim[1] = 32; inputDim[2] = 32;   // 入力画像の形状（チャンネル数、高さ、幅）

            // 読み込みたいデータセットのパス
            string filePath = @"C:\道路標識検出研究\ニューラルネット\入力データ\DatasetForTestNet";
            //------------------------------------------------

            FileManagement f = new FileManagement();


            // 処理時間計測用変数
            var sw = new System.Diagnostics.Stopwatch();

            // 一定数学習したときの値リスト
            List<double> train_acc = new List<double>();        // 認識精度(教師データ)
            List<double> test_acc = new List<double>();         // 認識精度(テストデータ)
            List<double> loss_list = new List<double>();        // 損失関数の値

            // 教師、訓練データ読み込み
            processingTeacher(filePath);

            // 正解ラベルをone-hot表現にする(1→[0,1,0,0,0,0,0,0,0,0])
            trainLabel = Calc.oneHot_Label(trainLabel, max:outPutSize);
            testLabel = Calc.oneHot_Label(testLabel, max:outPutSize);

            int trainSize = trainData.GetLength(0);  // 教師データの数
            int current_epoch = 0;                  // 何エポック終わったか
            int iter_per_epoch = Math.Max(trainSize / batchSize, 1);  // エポック

            // 処理時間計測開始
            sw.Start();

            // ネットワーク初期化
            Network network = new Network(inputDim, outPutSize: outPutSize, weightInitStd: 0.01, loadNet: false);

            // 更新回数学習
            for (int i = 0; i < iters_num; i++)
            {
                if (i != 0)
                {
                    Console.Write(".");
                }

                // ミニバッチ取得
                var resTuple = Calc.randomChoice(trainData, trainLabel, trainSize, batchSize);
                double[,,,] batchData = resTuple.Item1;
                double[,] batchLabel = resTuple.Item2;



                // 1エポック終わるごとに認識精度を確認
                if (i % iter_per_epoch == 0)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(current_epoch + " / " + (iters_num / iter_per_epoch) + " epoch finished");

                    // リストに損失関数、認識精度を追加
                    train_acc.Add(network.accuracy(trainData, trainLabel));
                    test_acc.Add(network.accuracy(testData, testLabel));
                    loss_list.Add(Calc.sum(network.loss(batchData, batchLabel)));

                    // コンソールに出力
                    Console.WriteLine("Accuracy train : " + train_acc[current_epoch]);
                    Console.WriteLine("Accuracy test  : " + test_acc[current_epoch]);
                    Console.WriteLine("loss : " + loss_list[current_epoch]);

                    current_epoch++;
                    Console.Write("Learning..");
                }
                // 誤差逆伝播法で勾配求める
                network.gradient(batchData, batchLabel);

                // パラメータ更新
                network.parameterUpdate(learning_rate);
            }

            // 処理時間計測停止
            sw.Stop();

            // ネットワークを保存
            network.saveNetwork(network.networkPath, network.networkName);

            // 損失関数、認識精度を出力
            f.save_List(train_acc, network.networkPath + "/" + network.networkName + "/LearningProcess", "TrainAccuracy");
            f.save_List(test_acc, network.networkPath + "/" + network.networkName + "/LearningProcess", "TestAccuracy");
            f.save_List(loss_list, network.networkPath + "/" + network.networkName + "/LearningProcess", "LossList");

            // 処理時間計測結果出力
            TimeSpan ts = sw.Elapsed;
            Console.WriteLine($"\n学習時間　{ts.Hours}時間 {ts.Minutes}分 {ts.Seconds}秒 {ts.Milliseconds}ミリ秒\n");

            // 更新回数やバッチサイズなどを出力
            f.outOption(learning_rate, iters_num, batchSize, inputDim, network.networkPath + "/" + network.networkName + "/LearningProcess", "option.txt");
        }


        /// <summary>
        /// 指定したファイルパスから教師、テスト画像を含むフォルダからデータ読み込みラベル作成
        /// </summary>
        public void processingTeacher(string filePath)
        {
            int[] fileNum = {0,0 }; // テストデータ、教師データの数、

            // 指定したパスの1つ下のサブフォルダ名取得
            string[] testTrainFolders = Directory.GetDirectories(filePath, "*");
            ImageData processing = new ImageData();

            // ファイル数取得
            for (int i = 0; i < testTrainFolders.Length; i++)
            {
                fileNum[i] = countData(testTrainFolders[i]);
                //Console.WriteLine(fileNum[i]);
            }

            Bitmap[] imagesTest = new Bitmap[fileNum[0]];
            Bitmap[] imagesTrain = new Bitmap[fileNum[1]];
            testLabel = new double[fileNum[0], 1];
            trainLabel = new double[fileNum[1], 1];
            int labelTest = 0;
            int labelTrain = 0;
            int countTest = 0;
            int countTrain = 0;

            // test > train の順に呼び出される
            for (int i = 0; i < testTrainFolders.Length; i++)
            {
                string[] subFolders2 = Directory.GetDirectories(testTrainFolders[i], "*"); 
                for (int j = 0; j < subFolders2.Length; j++)
                {
                    Console.Write("Reading  "+subFolders2[j]);
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(subFolders2[j]);

                    // bmpの画像の個数をカウント
                    System.IO.FileInfo[] files = di.GetFiles("*.bmp", System.IO.SearchOption.AllDirectories);
                    Console.WriteLine("　bmpの数"+files.Length);

                    for (int k = 0; k < files.Length; k++)
                    {
                        // 画像読み込み
                        Bitmap img = new Bitmap(Image.FromFile(@subFolders2[j] + "\\" + files[k].ToString()));

                        // 画像サイズ変換
                        Bitmap resizedImage = processing.resizeImage(img, inputDim[1], inputDim[2]);
                        if (i == 0)
                        {
                            imagesTest[countTest] = resizedImage;
                            testLabel[countTest, 0] = labelTest;
                            countTest++;
                        }
                        else if (i == 1)
                        {
                            imagesTrain[countTrain] = resizedImage;
                            trainLabel[countTrain, 0] = labelTrain;
                            countTrain++;
                        }
                        // リソース開放
                        img.Dispose();
                    }
                    if (i == 0)
                    {
                        labelTest++;
                    }
                    else if (i == 1)
                    {
                        labelTrain++;
                    }
                }
            }

            testData = processing.getRGB(imagesTest);
            trainData = processing.getRGB(imagesTrain);

            // リソース開放
            for (int i = 0; i <imagesTest.Length; i++)
            {
                imagesTest[i].Dispose();
            }
            for (int i = 0; i < imagesTrain.Length; i++)
            {
                imagesTrain[i].Dispose();
            }

        }

        private int countData(string directory) {
            int fileNum = 0;
            string[] subFolder = Directory.GetDirectories(directory, "*");
            for (int i = 0; i < subFolder.Length; i++)
            {
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(subFolder[i]);
                System.IO.FileInfo[] files = di.GetFiles("*.bmp", System.IO.SearchOption.AllDirectories);
                fileNum += files.Length;
            }
            return fileNum;
        }
         
    }
}
