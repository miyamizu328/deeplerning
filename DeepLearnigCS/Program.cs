﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Statistics;
using System.Drawing;
using System.Drawing.Imaging;

namespace DeepLearnigCS
{
    /// <summary>                                                                                             
    /// テスト用
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Trainer t = new Trainer();
            t.Learning();
        }
    }
}

