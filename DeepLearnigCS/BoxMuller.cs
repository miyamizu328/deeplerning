﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// ボックス＝ミュラー法によるガウス分布に従う乱数生成用クラス
    /// </summary>
    class BoxMuller
    {
        private Random random;
        private double mu;  // 平均の値
        private double sigma;   // 標準偏差の値
        private bool useCosine;  // cosを使うかどうか

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="mu">平均</param>
        /// <param name="sigma">標準偏差</param>
        /// <param name="useCosine">cosを使用するか（初期値：true）</param>
        public BoxMuller(double mu, double sigma, Boolean useCosine = true)
        {
            random = new Random();
            this.mu = mu;
            this.sigma = sigma;
            this.useCosine = useCosine;
        }

        /// <summary>
        /// 乱数生成
        /// </summary>
        /// <returns></returns>
        public double next()
        {
            double x = random.NextDouble();
            double y = random.NextDouble();
            double result;

            if (useCosine)
                result = Math.Sqrt(-2.0 * Math.Log(x)) * Math.Cos(2.0 * Math.PI * y);
            else
                result = Math.Sqrt(-2.0 * Math.Log(x)) * Math.Sin(2.0 * Math.PI * y);

            return result * sigma + mu;
        }

    }
}
