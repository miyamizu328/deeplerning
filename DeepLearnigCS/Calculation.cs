﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearnigCS
{
    /// <summary>
    /// 活性化関数、損失関数また配列の内積等の計算機能を持ったクラス
    /// </summary>
    public static class Calc
    {
        /// <summary>
        /// シグモイド関数
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[] sigmoid(double[] x)
        {
            double[] output = new double[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                output[i] = 1 / (1 + Math.Exp(-x[i]));
            }
            return output;
        }

        /// <summary>
        /// シグモイド関数
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,] sigmoid(double[,] x)
        {
            double[,] output = new double[x.GetLength(0), x.GetLength(1)];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    output[i, j] = 1 / (1 + Math.Exp(-x[i, j]));
                }
            }
            return output;
        }

        /// <summary>
        /// シグモイド関数
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,,,] sigmoid(double[,,,] x)
        {
            double[,,,] output = new double[x.GetLength(0), x.GetLength(1), x.GetLength(2), x.GetLength(3)];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        for (int l = 0; l < x.GetLength(3); l++)
                        {
                            output[i, j, k, l] = 1 / (1 + Math.Exp(-x[i, j, k, l]));
                        }
                    }
                }
            }
            return output;
        }




        /// <summary>
        /// ステップ関数
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double step(double x)
        {
            if (x > 0)
                return 1;
            return 0;
        }

        /// <summary>
        ///  relu関数
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double relu(double x)
        {
            return Math.Max(x, 0);
        }

        /// <summary>
        /// ソフトマックス関数
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static double[] softmax(double[] a)
        {
            double c = max(a);
            a = minus(a, c);   // オーバーフロー対策
            double[] exp_a = exp(a); // 指数関数
            double sum_exp_a = sum(exp_a);
            return division(exp_a, sum_exp_a);
        }

        /// <summary>
        /// ソフトマックス関数（2次元配列用）
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static double[,] softmax(double[,] a)
        {
            double[,] y = new double[a.GetLength(0), a.GetLength(1)];
            double[] tmp;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                tmp = softmax(oneFromTwo(a, i));
                for (int j = 0; j < tmp.Length; j++)
                {
                    y[i, j] = tmp[j];
                }
            }
            return y;
        }



        /// <summary>
        /// 2乗和誤差関数
        /// </summary>
        /// <param name="y"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static double mean_squared_error(double[] y, double[] t)
        {
            if (y.Length != t.Length)
            {
                Console.WriteLine("入力された値の要素数が一致してません。(mse)");
                return 0;
            }
            double[] tmp = new double[y.Length];
            for (int i = 0; i < y.Length; i++)
            {
                tmp[i] = Math.Pow((y[i] - t[i]), 2);
            }
            return 0.5 * sum(tmp);
        }

        /// <summary>
        /// 交差エントロピー誤差関数
        /// </summary>
        /// <returns></returns>
        public static double cross_entropy_error(double[] y, double[] t)
        {
            if (y.Length != t.Length)
            {
                Console.WriteLine("入力された値の要素数が一致してません。(cee)");
                return 0;
            }
            double delta = 1e-7; // log(0)させないための微小な値
            double[] tmp = new double[y.Length];
            for (int i = 0; i < y.Length; i++)
            {
                tmp[i] = (t[i] * Math.Log(y[i] + delta));
            }
            return -(Calc.sum(tmp));

        }

        /// <summary>
        /// 交差エントロピー誤差関数（2次元配列用）
        /// </summary>
        /// <returns></returns>
        public static double[] cross_entropy_error(double[,] x, double[,] t)
        {
            double[] loss = new double[x.GetLength(0)];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                loss[i] = Calc.cross_entropy_error(Calc.oneFromTwo(x, i), Calc.oneFromTwo(t, i));
            }
            return loss;
        }



        /// <summary>
        /// 関数の微分を求める
        /// </summary>
        /// <param name="f"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double numerical_diff(Func<double, double> f, double x)
        {
            double h = 1e-4;    // 0.0001
            return (f(x + h) - f(x - h)) / (2.0 * h);
        }

        /// <summary>
        /// 関数の偏微分を求める
        /// </summary>
        /// <param name="f"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[] numerical_gradient(Func<double[], double> f, double[] x)
        {
            double h = 1e-4;    // 0.0001

            // xと同じ形状の配列作成
            double[] grad = new double[x.Length];
            for (int i = 0; i < x.Length; i++)
                grad[i] = 0.0;

            for (int j = 0; j < x.Length; j++)
            {
                double tmp_val = x[j];

                // f(x+h)の計算
                x[j] = tmp_val + h;
                double fxh1 = f(x);

                // f(x-h)の計算
                x[j] = tmp_val - h;
                double fxh2 = f(x);

                grad[j] = (fxh1 - fxh2) / (2.0 * h);
                x[j] = tmp_val; // 値を元に戻す
            }
            return grad;
        }

        /// <summary>
        /// 関数の偏微分を求める（2次元配列対応版）
        /// </summary>
        /// <param name="f"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,] numerical_gradient(Func<double[], double> f, double[,] x)
        {
            double h = 1e-4;    // 0.0001
            double[] x1d = new double[x.GetLength(1)];  // ダミー
            // xと同じ形状の配列作成
            double[,] grad = new double[x.GetLength(0), x.GetLength(1)];

            for (int i = 0; i < x.GetLength(0); i++)
                for (int j = 0; j < x.GetLength(1); j++)
                    grad[i, j] = 0.0;

            // 要素ごとの偏微分を求める
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    double tmp_val = x[i, j];

                    // f(x+h)の計算
                    x[i, j] = tmp_val + h;
                    double fxh1 = f(x1d);

                    // f(x-h)の計算
                    x[i, j] = tmp_val - h;
                    double fxh2 = f(x1d);

                    grad[i, j] = (fxh1 - fxh2) / (2.0 * h);
                    x[i, j] = tmp_val; // 値を元に戻す
                }
            }
            return grad;
        }

        /// <summary>
        /// 勾配降下法によるパラメータの更新
        /// </summary>
        /// <param name="f">最適化したい関数</param>
        /// <param name="init_x">初期値</param>
        /// <param name="lr">学習率</param>
        /// <param name="step_num">勾配法による繰り返しの回数</param>
        /// <returns></returns>
        public static double[] gradient_descent(Func<double[], double> f, double[] init_x, double lr = 0.01, int step_num = 100)
        {
            double[] x = init_x;
            double[] grad;
            for (int i = 0; i < step_num; i++)
            {
                grad = numerical_gradient(f, x);
                for (int j = 0; j < x.Length; j++)
                {
                    x[j] -= lr * grad[j];
                }
            }
            return x;
        }
        /// <summary>
        /// 2次元配列に二次元配列を足す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] plus(double[,] x, double[,] b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] += b[i, j];
                }
            }
            return x;
        }

        /// <summary>
        /// 2次元配列にバイアス足す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] plusB(double[,] x, double[,] b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] += b[0, j];
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値を2次元配列全体に足す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] plus(double[,] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] += b;
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値を1次元配列全体に足す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] plus(double[] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                x[i] += b;
            }
            return x;
        }

        /// <summary>
        /// 入力された配列同士を足す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] plus(double[] x, double[] b)
        {
            if (x.Length != b.Length)
            {
                Console.WriteLine("要素数が一致してません(plus)");
                return x;
            }

            for (int i = 0; i < x.Length; i++)
            {
                x[i] = x[i] + b[i];
            }
            return x;
        }

        /// <summary>
        /// 2次元配列を2次元配列で引く
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] minus(double[,] x, double[,] b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] -= b[i, j];
                }
            }
            return x;
        }


        /// <summary>
        /// 2次元配列をバイアスで引く
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] minusB(double[,] x, double[,] b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] -= b[0, j];
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値で2次元配列全体を引く
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] minus(double[,] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] -= b;
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値を1次元配列全体を引く
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] minus(double[] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                x[i] -= b;
            }
            return x;
        }


        /// <summary>
        /// 入力された配列同士を引く
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] minus(double[] x, double[] b)
        {
            if (x.Length != b.Length)
            {
                Console.WriteLine("要素数が一致してません(plus)");
                return x;
            }
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = x[i] - b[i];
            }
            return x;
        }

        /// <summary>
        /// 入力された値を2次元配列全体に掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] multiply(double[,] x, double[,] b)
        {
            if (x.GetLength(0) != b.GetLength(0))
            {
                if (x.GetLength(1) != b.GetLength(1))
                {
                    Console.WriteLine("要素数が一致してません(multi)");
                    return x;
                }
            }

            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] *= b[i, j];
                }
            }
            return x;
        }


        /// <summary>
        /// 4次元配列同士掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,,,] multiply(double[,,,] x, double[,,,] b)
        {
            if (x.GetLength(0) != b.GetLength(0))
            {
                if (x.GetLength(1) != b.GetLength(1))
                {
                    if (x.GetLength(2) != b.GetLength(2))
                    {
                        if (x.GetLength(3) != b.GetLength(3))
                        {
                            Console.WriteLine("要素数が一致してません(multi)");
                            return x;
                        }
                    }
                            
                }
            }

            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        for (int l = 0; l < x.GetLength(3); l++)
                        {
                            x[i, j, k, l] *= b[i, j, k, l];
                        }
                    }
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値を4次元配列全体に掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,,,] multiply(double[,,,] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        for (int l = 0; l < x.GetLength(3); l++)
                        {
                            x[i, j, k, l] *= b;
                        }
                    }
                }
            }
            return x;
        }


        /// <summary>
        /// 入力された値を2次元配列全体に掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] multiply(double[,] x, double b)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] *= b;
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値を1次元配列全体に掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] multiply(double[] x, double b)
        {
            for (int i = 0; i < x.Length; i++)
            {
                x[i] *= b;
            }
            return x;
        }

        /// <summary>
        /// 入力された値を1次元配列同士を掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] multiply(double[] x, double[] b)
        {
            if (x.Length != b.Length)
            {
                Console.WriteLine("要素数が一致してません(multiply)");
                return x;
            }
            for (int i = 0; i < x.Length; i++)
            {
                x[i] *= b[i];
            }
            return x;
        }

        /// <summary>
        /// 入力された値で2次元配列全体を割る
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] division(double[,] x, double b)
        {
            if (b == 0)
            {
                Console.WriteLine("0で除算しようとしました");
                return x;
            }
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] /= b;
                }
            }
            return x;
        }

        /// <summary>
        /// 入力された値で1次元配列全体を割る
        /// </summary>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] division(double[] x, double b)
        {
            if (b == 0)
            {
                Console.WriteLine("0で除算しようとしました");
                return x;
            }
            for (int i = 0; i < x.Length; i++)
            {
                x[i] /= b;
            }
            return x;
        }

        /// <summary>
        /// 配列の内積（1次元配列と2次元配列）
        /// </summary>
        /// <param name="x"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static double[] dot(double[] x, double[,] w)
        {
            if (x.Length != w.GetLength(0))
            {
                Console.WriteLine("対応する次元の要素数が一致してません" + shape(x) + "," + shape(w));
                return x;
            }
            double[] output = new double[w.GetLength(1)];
            double tmp = 0;

            for (int i = 0; i < w.GetLength(1); i++)
            {
                for (int j = 0; j < x.Length; j++)
                {
                    tmp += x[j] * w[j, i];
                }
                output[i] = tmp;
                tmp = 0;
            }

            return output;
        }


        /// <summary>
        /// 配列の内積（2次元配列と1次元配列）
        /// </summary>
        /// <param name="x"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static double[] dot(double[,] x, double[] w)
        {
            if (x.GetLength(1) != w.Length)
            {
                Console.WriteLine("対応する次元の要素数が一致してません" + shape(x) + "," + shape(w));
                return w;
            }
            double[] output = new double[x.GetLength(0)];
            double tmp = 0;
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < w.Length; j++)
                {
                    tmp += x[i, j] * w[j];
                }
                output[i] = tmp;
                tmp = 0;
            }
            return output;
        }

        /// <summary>
        /// 配列の内積(２次元配列同士)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static double[,] dot(double[,] x, double[,] w)
        {
            if (x.GetLength(1) != w.GetLength(0))
            {
                Console.WriteLine("対応する次元の要素数が一致してません" + shape(x) + "," + shape(w));
                return x;
            }

            int c = 0;
            double[,] output = new double[x.GetLength(0), w.GetLength(1)];
            double tmp = 0;

            for (int i = 0; i < x.GetLength(0);)
            {
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    for (int k = 0; k < x.GetLength(1); k++)
                    {
                        tmp += x[i, k] * w[k, j];
                    }
                    output[i, c] = tmp;
                    tmp = 0;
                    c++;
                    if (c >= w.GetLength(1))
                    {
                        i++;
                        c = 0;
                    }
                }
            }
            return output;
        }

        /// <summary>
        /// 2次元配列全体に指数関数を掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,] exp(double[,] x)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] = Math.Exp(x[i, j]);
                }
            }
            return x;
        }

        /// <summary>
        /// 1次元配列全体に指数関数を掛ける
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[] exp(double[] x)
        {
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = Math.Exp(x[i]);
            }
            return x;
        }


        /// <summary>
        /// 2次元配列全体の和を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double sum(double[,] x)
        {
            double s = 0.0;
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    s += x[i, j];
                }
            }
            return s;
        }

        /// <summary>
        /// 1次元配列全体の和を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double sum(double[] x)
        {
            double s = 0.0;
            for (int i = 0; i < x.Length; i++)
            {
                s += x[i];
            }
            return s;
        }

        /// <summary>
        /// 特定の次元方向の要素の和を足して返す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="axis">0：行方向、1：列方向</param>
        /// <returns></returns>
        public static double[] sum(double[,] x, int axis)
        {
            double[] s;
            if (axis == 0)
            {
                s = zeros(x.GetLength(1));
                for (int i = 0; i < x.GetLength(0); i++)
                {
                    for (int j = 0; j < x.GetLength(1); j++)
                    {
                        s[j] += x[i, j];
                    }
                }
                return s;
            }
            else if (axis == 1)
            {
                s = zeros(x.GetLength(0));
                for (int i = 0; i < x.GetLength(0); i++)
                {
                    for (int j = 0; j < x.GetLength(1); j++)
                    {
                        s[i] += x[i, j];
                    }
                }
                return s;
            }
            Console.WriteLine("axisの指定が不正です");
            s = zeros(x.GetLength(1));
            return s;
        }


        public static double[,] max(double[,] x, int axis)
        {
            double max;
            double[,] o;
            int count = 0;
            // axisが1なら列方向の最大値を求める
            if (axis == 1)
            {
                o = new double[x.GetLength(0), 1];
                for (int i = 0; i < x.GetLength(0); i++)
                {
                    max = x[i, 0];
                    for (int j = 0; j < x.GetLength(1); j++)
                    {
                        if (max < x[i, j])
                        {
                            max = x[i, j];
                        }
                    }
                    o[count, 0] = max;
                    count++;
                }
            }
            // 行方向
            else
            {
                o = new double[1, x.GetLength(1)];
                for (int i = 0; i < x.GetLength(1); i++)
                {
                    max = x[0, i];
                    for (int j = 0; j < x.GetLength(0); j++)
                    {
                        if (max < x[j, i])
                        {
                            max = x[j, i];
                        }
                    }
                    o[count, 0] = max;

                    count++;
                }

            }
            return o;

        }



        /// <summary>
        /// 2次元配列の中から最大値を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double max(double[,] x)
        {
            double max = x[0, 0];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (max < x[i, j])
                    {
                        max = x[i, j];
                    }
                }
            }
            return max;
        }

        /// <summary>
        /// 1次元配列の中から最大値を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double max(double[] x)
        {
            double max = x[0];
            for (int i = 0; i < x.Length; i++)
            {
                if (max < x[i])
                {
                    max = x[i];
                }
            }
            return max;
        }

        /// <summary>
        /// 2次元配列の中から最小値を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double min(double[,] x)
        {
            double min = x[0, 0];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (min > x[i, j])
                    {
                        min = x[i, j];
                    }
                }
            }
            return min;
        }

        /// <summary>
        /// 1次元配列の中から最大値を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double min(double[] x)
        {
            double min = x[0];
            for (int i = 0; i < x.Length; i++)
            {
                if (min > x[i])
                {
                    min = x[i];
                }
            }
            return min;
        }

        /// <summary>
        /// 3次元配列の形状を返す
        /// </summary>
        /// <param name="x"></param>
        public static string shape(double[,,,] x)
        {
            return "(" + x.GetLength(0) + "," + x.GetLength(1) + "," + x.GetLength(2) + "," + x.GetLength(3) + ")";
        }

        /// <summary>
        /// 3次元配列の形状を返す
        /// </summary>
        /// <param name="x"></param>
        public static string shape(double[,,] x)
        {
            return "(" + x.GetLength(0) + "," + x.GetLength(1) + "," + x.GetLength(2) + ")";
        }

        /// <summary>
        /// 2次元配列の形状を返す
        /// </summary>
        /// <param name="x"></param>
        public static string shape(double[,] x)
        {
            return "(" + x.GetLength(0) + "," + x.GetLength(1) + ")";
        }

        /// <summary>
        /// 1次元配列の形状を返す
        /// </summary>
        /// <param name="x"></param>
        public static string shape(double[] x)
        {
            return "(" + x.Length + ",)";
        }

        /// <summary>
        /// 4次元配列の中身を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string display(double[,,,] x)
        {
            string result = "";
            //result = "[ \n";
            for (int i = 0; i < x.GetLength(0); i++)
            {
                result += "[[";
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (j == 0)
                        result += "[";

                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        if (j != 0)
                        {
                            result += "   ";
                        }
                        if (j == 0 && k != 0)
                            result += "   [";
                        else
                            result += "[";
                        for (int g = 0; g < x.GetLength(3); g++)
                        {
                            if (g == x.GetLength(3) - 1)
                            {

                                if (j == x.GetLength(1) - 1 && k == x.GetLength(2) - 1 && g == x.GetLength(3) - 1)
                                    result += x[i, j, k, g] + "]";
                                else
                                    result += x[i, j, k, g] + "]\n";
                            }
                            else
                                result += x[i, j, k, g] + ",";
                        }

                    }
                    if (j == x.GetLength(1) - 1)
                    {
                        result += "]]]\n";
                    }
                    else
                        result += "\n";
                }
                result += "\n";
            }
            return result;
        }


        /// <summary>
        /// 3次元配列の中身を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string display(double[,,] x)
        {
            string result = "[";
            //result = "[ \n";
            for (int i = 0; i < x.GetLength(0); i++)
            {
                result += "[";
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (j == 0)
                        result += "[";
                    else
                    {
                        if (i == 0)
                        {
                            result += "  [";
                        }
                        else
                            result += " [";
                    }
                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        if (k == x.GetLength(2) - 1)
                            result += x[i, j, k] + "]";
                        else
                            result += x[i, j, k] + ",";
                    }
                    if (j == x.GetLength(1) - 1)
                    {
                        if (i == x.GetLength(0) - 1)
                            result += "]]";
                        result += "]\n\n";
                    }
                    else
                        result += "\n";
                }

            }
            return result;
        }



        /// <summary>
        /// 2次元配列の中身を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string display(double[,] x)
        {
            string result = "";
            //result = "[ \n";
            for (int i = 0; i < x.GetLength(0); i++)
            {
                result += "[";
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    if (j == x.GetLength(1) - 1)

                        result += x[i, j];
                    else
                        result += x[i, j] + ",";
                }

                if (i == x.GetLength(0) - 1)
                    result += "]";
                else
                    result += "] \n";
            }
            //result += " ]";
            return result;
        }

        /// <summary>
        /// 1次元配列の中身を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string display(double[] x)
        {
            string result;
            result = "[";
            for (int i = 0; i < x.Length; i++)
            {
                if (i == x.Length - 1)
                    result += x[i];
                else
                    result += x[i] + ",";
            }
            result += "]";
            return result;
        }

        /// <summary>
        /// 1次元配列の最大値のインデックスを返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static int argmax(double[] x)
        {
            int maxIndex = 0;
            double max = x[0];
            for (int i = 0; i < x.Length; i++)
            {
                if (max < x[i])
                {
                    max = x[i];
                    maxIndex = i;
                }
            }
            return maxIndex;
        }

        /// <summary>
        /// 2次元配列とインデックスから1次元配列を返す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double[] oneFromTwo(double[,] x, int index)
        {
            double[] output = new double[x.GetLength(1)];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = x[index, i];
            }

            return output;
        }

        /// <summary>
        /// 1次元配列を2次元配列に変換する
        /// </summary>
        /// <param name="x"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double[,] twoFromOne(double[] x)
        {
            double[,] output = new double[1, x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                output[0, i] = x[i];
            }

            return output;
        }


        /// <summary>
        /// 3次元配列とインデックスから2次元配列を返す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double[,] TwoFromThree(double[,,] x, int index)
        {
            double[,] output = new double[x.GetLength(1), x.GetLength(2)];
            for (int i = 0; i < output.GetLength(0); i++)
            {
                for (int j = 0; j < output.GetLength(1); j++)
                {
                    output[i, j] = x[index, i, j];
                }
            }
            return output;
        }


        /// <summary>
        /// 4次元配列とインデックスから3次元配列を返す
        /// </summary>
        /// <param name="x"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double[,,] threeFromFour(double[,,,] x, int index)
        {
            double[,,] output = new double[x.GetLength(1), x.GetLength(2), x.GetLength(3)];
            for (int i = 0; i < output.GetLength(0); i++)
            {
                for (int j = 0; j < output.GetLength(1); j++)
                {
                    for (int k = 0; k < output.GetLength(2); k++)
                    {
                        output[i, j, k] = x[index, i, j, k];
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// 0で初期化された1次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[] zeros(int size)
        {
            double[] o = new double[size];
            for (int i = 0; i < o.Length; i++)
                o[i] = 0.0;

            return o;
        }

        /// <summary>
        /// 0で初期化された2次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[,] zeros(int size1, int size2)
        {
            double[,] o = new double[size1, size2];
            for (int i = 0; i < o.GetLength(0); i++)
                for (int j = 0; j < o.GetLength(1); j++)
                    o[i, j] = 0.0;

            return o;
        }

        /// <summary>
        /// 0で初期化された4次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[,,,] zeros(int size1, int size2, int size3, int size4)
        {
            double[,,,] o = new double[size1, size2, size3, size4];
            for (int i = 0; i < o.GetLength(0); i++)
                for (int j = 0; j < o.GetLength(1); j++)
                    for (int k = 0; k < o.GetLength(2); k++)
                        for (int l = 0; l < o.GetLength(3); l++)
                            o[i, j, k, l] = 0.0;

            return o;
        }


        /// <summary>
        /// 0〜1 の一様乱数で初期化された1次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[] rand(int size)
        {
            System.Random r = new System.Random();
            double[] o = new double[size];
            for (int i = 0; i < o.Length; i++)
            {
                o[i] = r.NextDouble();
            }
            return o;
        }

        /// <summary>
        /// 0〜1 の一様乱数で初期化された2次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[,] rand(int row, int col)
        {
            System.Random r = new System.Random();
            double[,] o = new double[row, col];
            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    o[i, j] = r.NextDouble();
                }
            }
            return o;
        }

        /// <summary>
        /// 0〜1 の一様乱数で初期化された4次元配列を返す
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[,,,] rand(int batch_num, int channel, int row, int col)
        {
            System.Random r = new System.Random();
            double[,,,] o = new double[batch_num, channel, row, col];
            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    for (int k = 0; k < o.GetLength(2); k++)
                    {
                        for (int l = 0; l < o.GetLength(3); l++)
                        {
                            o[i, j, k, l] = r.NextDouble();
                        }
                    }
                }
            }
            return o;
        }


        /// <summary>
        /// ガウス分布で初期化された1次元配列を返す
        /// </summary>
        /// <param name="size">配列の長さ</param>
        /// <param name="mu">ガウス分布の平均</param>
        /// <param name="sigma">ガウス分布の標準偏差</param>
        /// <returns></returns>
        public static double[] randn(int size, double mu = 0, double sigma = 1.0)
        {
            BoxMuller b = new BoxMuller(mu, sigma);
            double[] o = new double[size];
            for (int i = 0; i < o.Length; i++)
            {
                o[i] = b.next();
            }

            return o;
        }

        /// <summary>
        /// ガウス分布で初期化された2次元配列を返す
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="mu">ガウス分布の平均</param>
        /// <param name="sigma">ガウス分布の標準偏差</param>
        /// <returns></returns>
        public static double[,] randn(int row, int col, double mu = 0, double sigma = 1.0)
        {
            BoxMuller b = new BoxMuller(mu, sigma);
            double[,] o = new double[row, col];
            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    o[i, j] = b.next();
                }
            }
            return o;
        }

        /// <summary>
        /// ガウス分布で初期化された4次元配列を返す
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="mu">ガウス分布の平均</param>
        /// <param name="sigma">ガウス分布の標準偏差</param>
        /// <returns></returns>
        public static double[,,,] randn(int num, int channel, int height, int width, double mu = 0, double sigma = 1.0)
        {
            BoxMuller b = new BoxMuller(mu, sigma);
            double[,,,] o = new double[num, channel, height, width];
            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    for (int k = 0; k < o.GetLength(2); k++)
                    {
                        for (int l = 0; l < o.GetLength(3); l++)
                        {
                            o[i, j, k, l] = b.next();
                        }
                    }

                }
            }
            return o;
        }

        /// <summary>
        /// Xavierの初期値(sigmoid使う場合推奨)
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="node_num">前層のノードの数</param>
        /// <returns></returns>
        public static double[,] xavier(int row, int col, int node_num)
        {
            double[,] w = randn(row, col, mu: 0, sigma: (1.0 / Math.Sqrt(node_num)));
            return w;
        }

        /// <summary>
        /// Heの初期値(Relu使う場合推奨)
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="node_num">前層のノードの数</param>
        /// <returns></returns>
        public static double[,] he(int row, int col, int node_num)
        {

            double[,] w = randn(row, col, mu: 0, sigma: (2.0 / Math.Sqrt(node_num)));
            return w;
        }

        /// <summary>
        /// Heの初期値(Relu使う場合推奨)
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="node_num">前層のノードの数</param>
        /// <returns></returns>
        public static double[,,,] he(int num, int channel, int row, int col, int node_num)
        {
            double[,,,] w = randn(num, channel, row, col, mu: 0, sigma: (2.0 / Math.Sqrt(node_num)));
            return w;
        }

        /// <summary>
        /// 4次元配列の転置
        /// </summary>
        /// <param name="x"></param>
        /// <param name="dim1">0が推奨</param>
        /// <param name="dim2"></param>
        /// <param name="dim3"></param>
        /// <param name="dim4"></param>
        /// <returns></returns>
        public static double[,,,] transpose(double[,,,] x, int dim1, int dim2, int dim3, int dim4)
        {
            double[,,,] o = new double[x.GetLength(dim1), x.GetLength(dim2), x.GetLength(dim3), x.GetLength(dim4)];
            int[] c = { 0, 0, 0, 0 };

            for (int b = 0; b < o.GetLength(0); b++)
            {
                for (int i = 0; i < o.GetLength(1); i++)
                {
                    for (int j = 0; j < o.GetLength(2); j++)
                    {
                        for (int k = 0; k < o.GetLength(3); k++)
                        {
                            o[b, i, j, k] = x[c[0], c[1], c[2], c[3]];
                            c[dim4]++;
                            if (c[dim4] >= x.GetLength(dim4))
                            {
                                c[dim4] = 0;
                                c[dim3]++;
                                if (c[dim3] >= x.GetLength(dim3))
                                {
                                    c[dim3] = 0;
                                    c[dim2]++;
                                    if (c[dim2] >= x.GetLength(dim2))
                                    {
                                        c[dim2] = 0;
                                        c[dim1]++;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return o;

        }


        /// <summary>
        /// 入力された2次元配列の転置を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,] transpose(double[,] x)
        {
            double[,] t = new double[x.GetLength(1), x.GetLength(0)];
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    t[j, i] = x[i, j];
                }
            }
            return t;
        }

        /// <summary>
        /// 入力された1次元配列の転置を返す
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double[,] transpose(double[] x)
        {
            double[,] t = new double[x.Length, 1];
            for (int i = 0; i < x.Length; i++)
            {
                t[i, 0] = x[i];
            }
            return t;
        }

        /// <summary>
        /// 教師ラベルをワンホット表現にする
        /// </summary>
        /// <param name="t">対象の教師データ</param>
        /// <param name="max">クラス分けの数</param>
        /// <returns></returns>
        public static double[,] oneHot_Label(double[,] t, int max = 10)
        {
            double[,] o = zeros(t.GetLength(0), max);
            for (int i = 0; i < t.GetLength(0); i++)
            {
                o[i, (int)t[i, 0]] = 1.0;
            }

            return o;
        }

        /// <summary>
        /// 入力された配列からランダムに配列を取得(ミニバッチ取得時使用)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="range"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Tuple<double[,], double[,]> randomChoice(double[,] x, double[,] t, int range, int size)
        {
            if (size >x.GetLength(0))
            {
                size = x.GetLength(0);
            }



            Random r1 = new Random();
            Random r2 = new Random(r1.Next());
            double[,] xb = zeros(size, x.GetLength(1));
            double[,] tb = zeros(size, t.GetLength(1));
            for (int i = 0; i < size; i++)
            {
                int index = r2.Next(range);
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    xb[i, j] = x[index, j];
                }
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    tb[i, j] = t[index, j];
                }
            }
            return Tuple.Create(xb, tb);
        }

        /// <summary>
        /// 入力された配列からランダムに配列を取得(ミニバッチ取得時使用)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="range"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Tuple<double[,,,], double[,]> randomChoice(double[,,,] x, double[,] t, int range, int size)
        {
            if (size > x.GetLength(0))
            {
                size = x.GetLength(0);
            }



            Random r1 = new Random();
            Random r2 = new Random(r1.Next());
            double[,,,] xb = zeros(size, x.GetLength(1),x.GetLength(2),x.GetLength(3));
            double[,] tb = zeros(size, t.GetLength(1));
            for (int i = 0; i < size; i++)
            {
                int index = r2.Next(range);
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    for (int k = 0; k < x.GetLength(2); k++)
                    {
                        for (int l = 0; l < x.GetLength(3); l++)
                        {
                            xb[i, j,k,l] = x[index, j,k,l];
                        }
                    }
                    
                }
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    tb[i, j] = t[index, j];
                }
            }
            return Tuple.Create(xb, tb);
        }






        /// <summary>
        /// パラメータと勾配を入力すると学習率に応じて更新
        /// </summary>
        /// <param name="x">パラメータ</param>
        /// <param name="dx">パラメータの勾配</param>
        /// <param name="learning_rate">学習率</param>
        /// <returns>更新後のパラメーター</returns>
        public static double[,] update(double[,] x, double[,] dx, double learning_rate)
        {
            double[,] W = new double[x.GetLength(0), x.GetLength(1)];
            for (int i = 0; i < W.GetLength(0); i++)
            {
                for (int j = 0; j < W.GetLength(1); j++)
                {
                    W[i, j] = x[i, j] - (learning_rate * dx[i, j]);
                }
            }
            return W;
        }

        /// <summary>
        /// パラメータと勾配を入力すると学習率に応じて更新
        /// </summary>
        /// <param name="x">パラメータ</param>
        /// <param name="dx">パラメータの勾配</param>
        /// <param name="learning_rate">学習率</param>
        /// <returns>更新後のパラメーター</returns>
        public static double[,,,] update(double[,,,] x, double[,,,] dx, double learning_rate)
        {
            double[,,,] W = new double[x.GetLength(0), x.GetLength(1), x.GetLength(2), x.GetLength(3)];
            for (int i = 0; i < W.GetLength(0); i++)
            {
                for (int j = 0; j < W.GetLength(1); j++)
                {
                    for (int k = 0; k < W.GetLength(2); k++)
                    {
                        for (int l = 0; l < W.GetLength(3); l++)
                        {
                            W[i, j, k, l] = x[i, j, k, l] - (learning_rate * dx[i, j, k, l]);
                        }
                    }
                }
            }
            return W;
        }


        /// <summary>
        /// パディング
        /// </summary>
        /// <param name="x">パディング対象</param>
        /// <param name="pad">パディングの幅</param>
        /// <returns></returns>
        public static double[,] padding(double[,] x, int pad)
        {
            if (pad == 0)
            {
                return x;
            }
            double[,] o = new double[x.GetLength(0) + (2 * pad), x.GetLength(1) + (2 * pad)];

            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    if ((i >= pad && i < pad + x.GetLength(0)) && (j >= pad && j < pad + x.GetLength(1)))
                    {
                        o[i, j] = x[i - pad, j - pad];
                    }
                    else
                    {
                        o[i, j] = 0.0;
                    }
                }
            }
            return o;
        }

        /// <summary>
        /// パディング(4次元配列版)
        /// </summary>
        /// <param name="x">パディング対象</param>
        /// <param name="pad">パディングの幅</param>
        /// <returns></returns>
        public static double[,,,] padding(double[,,,] x, int pad)
        {
            if (pad == 0)
            {
                return x;
            }
            double[,,,] o = new double[x.GetLength(0), x.GetLength(1), x.GetLength(2) + (2 * pad), x.GetLength(3) + (2 * pad)];
            for (int k = 0; k < o.GetLength(0); k++)
            {
                for (int h = 0; h < o.GetLength(1); h++)
                {
                    for (int i = 0; i < o.GetLength(2); i++)
                    {
                        for (int j = 0; j < o.GetLength(3); j++)
                        {
                            if ((i >= pad && i < pad + x.GetLength(2)) && (j >= pad && j < pad + x.GetLength(3)))
                            {
                                o[k, h, i, j] = x[k, h, i - pad, j - pad];
                            }
                            else
                            {
                                o[k, h, i, j] = 0.0;
                            }
                        }
                    }
                }
            }
            return o;
        }

        /// <summary>
        /// 4次元のデータを2次元に変換
        /// </summary>
        /// <param name="x">(バッチサイズ、チャンネル、高さ、横幅)</param>
        /// <param name="filter_h">フィルターの高さ</param>
        /// <param name="filter_w">フィルターの幅</param>
        /// <param name="stride">ストライド</param>
        /// <param name="pad">パディング</param>
        /// <returns></returns>
        public static double[,] im2col(double[,,,] x, int filter_h, int filter_w, int stride = 1, int pad = 0)
        {
            // 着目点の位置
            int tarH = 0;
            int tarW = 0;
            int tar = 0;
            int count = 0;

            int out_h = (1) + (x.GetLength(2) + (2 * pad) - filter_h) / stride;
            int out_w = (1) + (x.GetLength(3) + (2 * pad) - filter_w) / stride;

            // 入力をパディング適用した配列を作成
            double[,,,] tmp = Calc.padding(x, pad);

            double[,] o = new double[tmp.GetLength(0) * out_h * out_w, tmp.GetLength(1) * filter_h * filter_w];
            //int t = (tmp.GetLength(2) - filter_h + 1) * (tmp.GetLength(3) - filter_w + 1) / stride;
            int t = o.GetLength(0) / x.GetLength(0);

            for (int b = 0; b < tmp.GetLength(0); b++)  // バッチサイズだけ繰り返す
            {
                while (tarH + filter_h <= tmp.GetLength(2) && tarW + filter_w <= tmp.GetLength(3))   // ひとつのデータの要素分繰り返す
                {
                    for (int c = 0; c < tmp.GetLength(1); c++)   // チャンネルの数だけ繰り返す
                    {
                        for (int h = tarH; h < tarH + filter_h; h++)    // フィルターの幅だけ繰り返す
                        {
                            for (int w = tarW; w < tarW + filter_w; w++)    // フィルターの高さだけ繰り返す
                            {
                                o[(t * b) + tar, count] = tmp[b, c, h, w];
                                count++;
                                if (count >= tmp.GetLength(1) * filter_h * filter_w)
                                {
                                    count = 0;
                                }
                            }
                        }
                    }
                    if (tarW + stride + filter_w > tmp.GetLength(3))
                    {
                        tarW = 0;
                        tarH += stride; // 着目点の高さをずらす
                    }
                    else
                    {
                        tarW += stride; // 着目点を横にずらす    
                    }
                    tar++;
                }
                // 着目点初期化
                tar = 0;
                tarH = 0;
                tarW = 0;
            }
            return o;
        }


        /// <summary>
        /// im2colの逆
        /// </summary>
        /// <param name="x">(バッチサイズ、チャンネル、高さ、横幅)</param>
        ///  <param name="input_shape">形状</param>
        /// <param name="filter_h">フィルターの高さ</param>
        /// <param name="filter_w">フィルターの幅</param>
        /// <param name="stride">ストライド</param>
        /// <param name="pad">パディング</param>
        /// <returns></returns>
        public static double[,,,] col2im(double[,] x, int[] input_shape, int filter_h, int filter_w, int stride = 1, int pad = 0)
        {
            int N = input_shape[0];
            int C = input_shape[1];
            int H = input_shape[2];
            int W = input_shape[3];

            // 出力の着目点の位置
            int tarN = 0;
            int tarC = 0;
            int tarH = 0;
            int tarW = 0;
            int startH = 0;
            int startW = 0;

            // 元のデータの位置とチャンネルの位置
            int currentH = 0;
            int currentW = 0;

            int out_h = (1) + (H + (2 * pad) - filter_h) / stride;
            int out_w = (1) + (W + (2 * pad) - filter_w) / stride;

            double[,,,] img = zeros(N, C, H + (2 * pad) + stride - 1, W + (2 * pad) + stride - 1);

            for (int w = 0; w < x.GetLength(1); w++)
            {
                for (int h = 0; h < x.GetLength(0); h++)
                {
                    // 別のデータの領域に入ったとき
                    if (h != 0 && (h - currentH) % (x.GetLength(0) / N) == 0)
                    {
                        currentH = h;
                        tarN++;

                        tarW = 0;
                        tarH = 0;

                    }
                    // 別のチャンネルに入ったとき
                    if ((w - currentW) >= (x.GetLength(1) / C))
                    {
                        currentW = w;
                        tarC++;
                        if (tarC >= img.GetLength(1))
                        {
                            tarC = 0;
                        }
                    }
                    // 出力の幅を超えるとき
                    if (startW + out_w > img.GetLength(3))
                    {
                        startW = 0;
                        startH++;
                        // 出力の高さを超えるとき
                        if (startH + out_h > img.GetLength(2))
                        {
                            startH = 0;
                        }
                    }
                    img[tarN, tarC, tarH + startH, tarW + startW] += x[h, w];
                    tarW++;
                    if (tarW >= out_w)
                    {
                        tarW = 0;
                        tarH++;
                        if (tarH >= out_h)
                        {
                            tarH = 0;
                        }
                    }
                }
                startW++;
                currentH = 0;
                tarN = 0;
            }
            return img;
        }


        /// <summary>
        /// 二次元配列を入力したサイズに応じて広くまたは狭くする（フィルター展開）
        /// </summary>
        /// <param name="FN">フィルターの個数（二つの引数のうち必ず一つ-1にしてください）</param>
        /// <param name="size">横幅</param>
        /// <returns></returns>
        public static double[,] reshape(double[,] x, int FN, int size = -1)
        {
            double[,] o;
            if (size == -1)
            {
                o = new double[FN, (x.GetLength(0) * x.GetLength(1)) / FN];
            }
            else
            {
                o = new double[(x.GetLength(0) * x.GetLength(1)) / size, size];
            }
            // 入力と出力の総要素数が一致してなければエラー
            if (x.Length != o.Length)
            {
                Console.WriteLine("引数が不適切です");
            }


            int countRow = 0;   // 列方向のカウント   
            int countCol = 0;   // 行方向のカウント
            for (int i = 0; i < o.GetLength(0); i++)
            {
                for (int j = 0; j < o.GetLength(1); j++)
                {
                    if (countCol >= x.GetLength(1))
                    {
                        countRow++;
                        countCol = 0;
                    }

                    o[i, j] = x[countRow, countCol];
                    countCol++;

                }
            }
            return o;
        }

        /// <summary>
        /// 二次元配列を四次元配列にする
        /// </summary>
        /// <param name="FN">フィルターの個数</param>
        /// <param name="size">横幅</param>
        /// <returns></returns>
        public static double[,,,] reshape(double[,] x, int FN, int size1, int size2, int size3)
        {
            double[,,,] o;
            if (size1 == -1)
            {
                o = new double[FN, x.Length / (FN * size2 * size3), size2, size3];
            }
            else if (size2 == -1)
            {
                o = new double[FN, size1, x.Length / (FN * size1 * size3), size3];
            }
            else if (size3 == -1)
            {
                o = new double[FN, size1, size2, x.Length / (FN * size1 * size2)];
            }
            else
            {
                o = new double[FN, size1, size2, size3];
            }
            // 入力と出力の総要素数が一致してなければエラー
            if (x.Length != o.Length)
            {
                Console.WriteLine("引数が不適切です");
            }


            int countRow = 0;   // 列方向のカウント   
            int countCol = 0;   // 行方向のカウント
            for (int b = 0; b < o.GetLength(0); b++)
            {
                for (int i = 0; i < o.GetLength(1); i++)
                {
                    for (int j = 0; j < o.GetLength(2); j++)
                    {
                        for (int k = 0; k < o.GetLength(3); k++)
                        {
                            if (countCol >= x.GetLength(1))
                            {
                                countRow++;
                                countCol = 0;
                            }
                            o[b, i, j, k] = x[countRow, countCol];
                            countCol++;
                        }
                    }
                }
            }
            return o;
        }

        /// <summary>
        /// 四次元配列を二次元に展開（フィルター展開）
        /// </summary>
        /// <param name="x">展開対象</param>
        /// <param name="FN">二つの引数うちひとつは-1にするのを推奨</param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static double[,] reshape(double[,,,] x, int FN, int size = -1)
        {
            double[,] o;
            if (FN == -1)
            {
                o = new double[x.Length / size, size];
            }
            else if (size == -1)
            {
                o = new double[FN, x.Length / FN];
            }
            else
            {
                o = new double[FN, size];
            }

            // 入力と出力の総要素数が一致してなければエラー
            if (x.Length != o.Length)
            {
                Console.WriteLine("引数が不適切です");
            }

            int countRow = 0;   // 列方向のカウント   
            int countCol = 0;   // 行方向のカウント

            for (int b = 0; b < x.GetLength(0); b++)
            {
                for (int i = 0; i < x.GetLength(1); i++)
                {
                    for (int j = 0; j < x.GetLength(2); j++)
                    {
                        for (int k = 0; k < x.GetLength(3); k++)
                        {
                            if (countCol >= o.GetLength(1))
                            {
                                countRow++;
                                countCol = 0;
                            }
                            o[countRow, countCol] = x[b, i, j, k];
                            countCol++;
                        }
                    }
                }
            }
            return o;
        }
    }
}
